<?php

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->name,
);

?>

<h1>View Product #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cat_id',
		'name',
		'code',
		'price',
		'size',
		'user_id',
		'image',
		'status',
		'date_added',
		'date_modified',
	),
)); ?>
