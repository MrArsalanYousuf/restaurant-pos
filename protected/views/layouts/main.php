<?php 
$acc = Accessories::model()->find();
$favi = $acc->favi_image;
$logo = $acc->logo;
$title = $acc->title;
$footer = $acc->footer_name;
?>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $acc->title; ?></title>


  <script src="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/js/jquery.js"></script>

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/core-style.css" />

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/tooltipster.bundle.min.css" />

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/tooltipster-sideTip-punk.min.css" />

  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/bootstrap.min.css">

  <!-- Link autocomplete -->
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/jquery-auto-complete.css" rel="stylesheet" media="screen">

  <!-- Custom CSS -->
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/simple-sidebar.css" rel="stylesheet">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/font-awesome/css/font-awesome.min.css">

  <!-- toast notification -->  
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/css/toastem.css">

  <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl .'/admin/images/logo/'.$favi; ?>"/>

</head>

<div id="toastem"></div>

<body>

  <!-- Navination Bar -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#" style="color: #FFF; font-size: 35px; font-family: Brush Script MT, Brush Script Std, cursive"><?php echo $title; ?></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <form class="navbar-form navbar-left">
          <div class="form-group">
            <input type="text" class="form-control advance_search" placeholder="Search">
          </div>
          <a class="btn btn-info fa fa-search advance_search_btn" style="padding: 10px 25px"></a>
        </form>

        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #FFF"><?php echo Yii::app()->user->name; ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>"><i class='fa fa-arrow-right'></i> Log Out </a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

  <div class="divider" style="padding-top: 50px;"></div>

  <div class="col-md-12 main-content" id="products">

    <?php echo $content; ?>

  </div>

  <div style='display:none'>
    <button class="btn scs" type="button" name="button" id="success-alert">Success Alert</button>
    <button class="btn scs" type="button" name="button" id="error-alert">Error Alert</button>
    <button class="btn scs" type="button" name="button" id="normal-alert">Normal Alert</button>
  </div>

  <script src="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/js/toastem.js"></script>

  <div class='ppp'></div>

  <script src="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/js/bootstrap.min.js"></script>

  <!-- jQuery autocomplete -->
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/js/jquery-auto-complete.js"></script>

  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/arsalan/js/tooltipster.bundle.min.js"></script>


  <!-- Menu Toggle Script -->
  <script>

// $(document).ready(function (){
//   $("#menu-toggle2").click();
// });

function openModal(){
  $('#customerModal').modal();
}

$("#myModal").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});


$("#printModal").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});


$("#customerModal").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});

$("#menu-toggle").click(function(e) {

  e.preventDefault();

  $("#wrapper").toggleClass("toggled");

});

$("#menu-toggle2").click(function(e) {

  e.preventDefault();

  $("#wrapper2").toggleClass("toggled");

});

$("#menu-toggle3").click(function(e) {

  e.preventDefault();

  var a = $(this).attr('#wrapper2');
  var a = $("#sidebar-wrapper").width();

  if(a==250){
    $('#wrapper2').addClass('toggled');
  }
  else if(a<=125){
    $('#wrapper2').removeClass('toggled');
  }

});

</script>

</body>

</html>
