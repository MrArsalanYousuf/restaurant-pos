<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>POS</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/arsalan/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/arsalan/css/font-awesome/css/font-awesome.min.css">
    <!-- Custom Theme Style -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/arsalan/css/custom.min.css" rel="stylesheet">

<style type="text/css">
.login_form {
    border: 3px solid #f1f1f1;
    background: #FFF;
    padding: 15px;
    padding-bottom: 0px;
    padding-top: 1px;
}
.btn-danger{
    width: 115px;
  }
  .login_content form input[type="submit"], #content form .submit{
    margin-left: 44px !important;
    float: none !important;
  }
  label{
    color: #000;
  }
  .login_content{
    text-shadow: none !important;
  }
  .login_content h1{
    color: #000 !important;
  }
  p{
    color: #000 !important;
    font-size: 10px;
  }
@media (min-width: 360px) {

#logo{
 width:40%;
}

}

@media (min-width: 1000px) {

#logo{
 width:20%;
}

}
</style>

  </head>

<body class="login" style="background-image: url('../../admin/images/background/1802059.jpg');background-size: 100% 100%;background-repeat: no-repeat;">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

<div class="logo text-center">
<!--   <a href="<?php echo Yii::app()->CreateUrl("site/login"); ?>">
  <img id="logo" src="<?php echo Yii::app()->request->baseUrl;?>/images/logo/logo.png" alt=""/>
  </a> -->
</div>

      <div class="login_wrapper">

        <div class="animate form login_form">
          <section class="login_content">

<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'users-form',
  'enableAjaxValidation'=>false,
    
)); ?>
            <form>
              <h1>Login Form</h1>
              <div>
                <?php echo $form->labelEx($model,'username',array('id'=>'formTxt')); ?>
                <?php echo $form->textField($model,'username',array('class'=>'form-control','size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'username'); ?>
              </div>
              <div>
                <?php echo $form->labelEx($model,'password',array('id'=>'formTxt')); ?>
                <?php echo $form->passwordField($model,'password',array('class'=>'form-control','size'=>60,'maxlength'=>256)); ?>
                <?php echo $form->error($model,'password'); ?>
              </div>
              <div>
                <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-danger btn-sumbit')); ?>
                <a class="reset_pass" href="#"></a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signup" class="to_register"> </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                   <p>All Rights Reserved. POS Design by <a href="javascript:void(0)" style="color: red">Arsalan Yousuf</a></p>
                </div>
              </div>
            </form>
          <?php $this->endWidget(); ?>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>