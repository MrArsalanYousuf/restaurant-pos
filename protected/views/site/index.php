<?php
date_default_timezone_set("Asia/Karachi");
$date = new DateTime();

$acc = Accessories::model()->find();
$favi = $acc->favi_image;
$logo = $acc->logo;
?>

<div class="row">
    <div class="container-fluid">
    <div class="col-md-2 col-sm-2 col-xs-2">
    
    </div>

    <div class="col-md-5 col-sm-5 col-xs-5">
      <!-- <span style="font-size:30px;cursor:pointer" id="menu-toggle2" class="menu-toggle2">&#9776;</span> -->
    </div>

    <div class="col-md-5 col-sm-5 col-xs-5">
      <h2 style="font-size: 20px;font-weight: 700;color: #333;text-align: right" id="date_time"></h2>
    </div>
  </div>
</div>


<div class="row" id="products-listing">
    <div class="col-md-2 col-sm-2 col-xs-2"> 
      <div id="wrapper2">
      <div id="sidebar-wrapper" class="sidebar">
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
          <ul class="nav sidebar-nav">

            <li>
              <a href="javascript:void(0)" class="fil-cat" data-rel="all">All</a>
            </li>

            <?php foreach ($cats as $key => $value) { ?>

              <li>
                <a href="javascript:void(0)" class="fil-cat" data-rel="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></a>
              </li>

            <?php } ?>

            <li style="display: none">
              <a href="javascript:void(0)" class="fil-cat search" data-rel="search">Search</a>
            </li>

          </ul>
        </nav>
      </div>
      </div>
    </div>

    <div class="col-md-7 col-sm-6 col-xs-6"> 
      <div class="tab" role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" order-type="Dine in" class="active"><a href="#dinein" class="dinein" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-cutlery"></i> Dine In</a></li>

          <li role="presentation" order-type="Takeaway"><a href="#takeaway" class="takeaway" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-shopping-bag"></i> Take Away</a></li>

          <li role="presentation" order-type="Delivery"><a href="#delivery" class="delivery" aria-controls="messages" role="tab" data-toggle="tab" onclick="openModal()"><i class="fa fa-motorcycle"></i> Delivery</a></li>
        </ul>

        <div class="tab-content tabs">

          <div role="tabpanel" class="tab-pane fade in active" id="dinein">
            <div id="portfolio" class="gallary">

              <?php foreach ($mp as $key => $value) {

                if($value['size'] == 'Yes') { ?>

                  <div class="col-md-3 col-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                    <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' class="add_multi_product">

                      <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                      <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                      <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                    </a>

                  </div>

                <?php } else { ?>

                  <div class="col-md-3 xol-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                    <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' data-name='<?php echo $value['name'] ?>' data-price='<?php echo $value['price'] ?>' data-image='<?php echo $value['image'] ?>' class="add_product">

                      <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                      <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                      <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                    </a>

                  </div>

                <?php } } ?>

                <div class="col-md-12 col-sm-12 col-xs-12 search-div-here">

                </div>

              </div>

            </div>




            <div role="tabpanel" class="tab-pane fade" id="takeaway">
              <div id="portfolio" class="gallary">

                <?php foreach ($mp as $key => $value) {

                  if($value['size'] == 'Yes') { ?>

                    <div class="col-md-3 col-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                      <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' class="add_multi_product">

                        <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                        <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                        <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                      </a>

                    </div>

                  <?php } else { ?>

                    <div class="col-md-3 col-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                      <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' data-name='<?php echo $value['name'] ?>' data-price='<?php echo $value['price'] ?>' data-image='<?php echo $value['image'] ?>' class="add_product">

                        <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                        <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                        <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                      </a>

                    </div>

                  <?php } } ?>

                  <div class="col-md-12 col-sm-12 col-xs-12 search-div-here">

                  </div>

                </div>
              </div>




              <div role="tabpanel" class="tab-pane fade" id="delivery">
                <div id="portfolio" class="gallary">

                  <?php foreach ($mp as $key => $value) {

                    if($value['size'] == 'Yes') { ?>

                      <div class="col-md-3 col-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                        <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' class="add_multi_product">

                          <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                          <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                          <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                        </a>

                      </div>

                    <?php } else { ?>

                      <div class="col-md-3 col-sm-6 col-xs-6 product scale-anm tooltips <?php echo $value['cat_id'] ?> all" title="<?php echo $value['name'] ?>">

                        <a href="javascript:void(0)" style="text-decoration:none" data-id='<?php echo $value['id'] ?>' data-cat-id='<?php echo $value['cat_id'] ?>' data-name='<?php echo $value['name'] ?>' data-price='<?php echo $value['price'] ?>' data-image='<?php echo $value['image'] ?>' class="add_product">

                          <h4 style="font-size: 14px;font-weight: 600"><?php echo substr($value['name'],0,15).'...';  ?></h4>
                          <h5><span class="label label-danger">Price : <?php echo $value['price'] ?></span></h5>
                          <img src="<?php echo Yii::app()->baseUrl .'/admin/images/products/'.$value['image']; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg'" class="thumbnail img-responsive" style="min-width: 100%;height: 150px" />

                        </a>

                      </div>

                    <?php } } ?>

                    <div class="col-md-12 col-sm-12 col-xs-12 search-div-here">

                    </div>

                  </div>
                </div>


              </div>
            </div> <!-- Nav Tabs End -->

          </div><!-- col-md-8 -->


          <!-- Side Panel Start -->

          <div class="col-md-3 col-sm-4 col-xs-4" id="side_panel" style="margin-top: 10px">

            <div class="panel panel-default" id="order_panel">

              <div class="panel-heading">

                <div class="row">

                 <div class="col-md-6 col-sm-6 col-xs-6">
                  <h4>ITEMS</h4>    
                </div>


                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                  <h4><span class="label label-info item-count">0</span></h4>   
                </div>

              </div>

            </div>

            <div class="panel-body">

              <div class="order-body" style="max-height:350px; overflow:auto;">


              </div>

              <hr />

              <div class="row">

               <div class="col-md-6 col-sm-6 col-xs-6">
                 <h5>Subtotal:</h5>
               </div>

               <div class="col-md-6 col-sm-6 col-xs-6 text-right ">
                 <h5>PKR <strong class="subtotal">0.00</strong></h5>
               </div>

             </div>

             <div class="row">

               <div class="col-md-6 col-sm-6 col-xs-6">
                 <h5 class="discount_label">Discount:</h5>
               </div>

               <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                 <h5>PKR <strong class="discount">0.00</strong></h5>
               </div>

             </div>

             <div class="row">

              <div class="col-md-6 col-sm-6 col-xs-6">
                <h5 class="tax_label">GST:</h5>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                <h5>PKR <strong class="tax">0.00</strong></h5>
              </div>

            </div>

            <hr/>

            <div class="row">

             <div class="col-md-6 col-sm-6 col-xs-6" style="display: table; height: 48px; overflow: hidden;">
              <div style="display: table-cell; vertical-align: middle;font-weight: 600">
               <div>Grand Total:</div>
             </div>
           </div>

           <div class="col-md-6 col-sm-6 col-xs-6 text-right">
             <h5 style="font-size: 20px">PKR <strong class="totalamount">0.00</strong></h5>
           </div>

         </div>


         <div class="pdng table-waiter">
          <div class="row">
            <div class="col-md-12 col-xs-12">
              <select class="form-control" id="tables">
               <option value="">Select Table</option>
               <?php if(!empty($tables)){
                foreach ($tables as $k => $v){ ?>
                  <option value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
                <?php } } ?>
              </select>
            </div>

            <div class="col-md-12 col-xs-12" style="margin-top: 2%">
              <select class="form-control" id="waiters">
               <option value="">Select Waiter</option>
               <?php if(!empty($waiters)){
                foreach ($waiters as $k => $v){ ?>
                  <option value="<?php echo $v['id']; ?>"><?php echo $v['first_name']." ".$v['last_name']; ?></option>
                <?php } } ?>
              </select>
            </div>
          </div>
        </div>

        <div class="pdng additional-information" style="display: none">
          <div class="row">
            <div class="col-md-12 col-xs-12">
              <label>Additional Information</label>
              <textarea class="form-control" rows="2" id="additional-information"></textarea>
            </div>
          </div>
        </div>

        <div class="pdng">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 order" style="display: none">
             <a href="javascript:void(0)" class="btn btn-default" id="order_btn">CHECK OUT</a>
           </div>
         </div>
       </div>

     </div>

   </div>

 </div>

 <!-- <div style="clear:both;"></div> -->
</div> <!-- container -->


<?php include('modal/product_size.php'); ?>


<?php include('modal/print_slip.php'); ?>


<?php include('modal/customer.php'); ?>


<?php include('site_script/script.php'); ?>