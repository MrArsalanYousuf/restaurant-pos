<style type="text/css">
  button.close {
    -webkit-appearance: none;
    background: #5bc0de;
    border: 1px solid #5bc0de;
    cursor: pointer;
    padding: 2px 6px 4px 6px;
  }
  .close{
    color: #FFF;
    font-size: 1.3em;
    line-height: 1.2em;
    opacity: 1;
    position: absolute;
    right: 15px;
    top: 15px;
    font-weight: 900;
  }
</style>

<!-- Modal Start Here --> 
<div class="modal fade-scale" id="myModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #696969;color: #FFF;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" style="font-size: 18px">Select Size</h3>
      </div>
      <div class="modal-body">

        <div class="container-fluid" id="remain_product">

        </div>

      </div>
    </div>

  </div>
  </div> <!-- Modal End Here -->