<style type="text/css">
  @media print {
    div.break {page-break-after: always;}
  }
  .table>tbody>tr>td, .table>tbody>tr>th {
    border-top: none !important;
  }
  .separator-start{
    height: 20px;
  }
  .separator-end{
    height: 20px;
  }
</style>

<?php $acc = Accessories::model()->find();
$favicon = $acc->favi_image;
$logo = $acc->logo;
$checkout = Checkout::model()->find(array('order'=>'id DESC'));
$receipt_no = $checkout->id+1;
?>

<div class="modal fade-scale" id="printModal" data-keyboard="false" data-backdrop="static" style="display: none">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-md-12 col-sm-12 col-xs-12 text-center" id="printableArea">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <img src="<?php echo Yii::app()->baseUrl .'/admin/images/logo/'.$logo; ?>" alt="<?php echo $acc->title; ?>" class="img-responsive center-block">
              </div>
              
              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h5 style="text-transform: uppercase;"><?php echo $acc->address; ?></h5>
                <h5>PH: <?php echo $acc->phone_no; ?> &nbsp;&nbsp;MOB: <?php echo $acc->mobile_no; ?></h5>

                <br>

                <h5><?php echo date('d-M-Y h:i A'); ?></h5>
                <h4 style="font-weight: 600;font-size: 22px">Receipt No. <?php echo "000".$receipt_no; ?></h4>
                <h5><?php echo $checkout->order_type; ?></h5>
                <h5>Pre Payment Bill</h5>
              </div>

            </div> <!-- row -->

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                <table id="datatable" class="table">

                  <thead class="black white-text">

                    <tr>

                      <th class="text-left" scope="col" style="width:10%;">
                        Qty
                      </th>

                      <th scope="col" style="width:50%;">
                        Particulars
                      </th>

                      <th class="text-right" scope="col" style="width:20%;">
                        Rate
                      </th>

                      <th class="text-right" scope="col" style="width:20%;">
                        Amount
                      </th>

                    </tr>
                  </thead>

                  <tbody class='Personal_info-tbody productSalePrint'>

                  </tbody>

                  <tbody class='Personal_info-tbody' style="border-bottom: 2px solid #ddd">

                    <tr>
                      <th colspan="2">Sub Total:</th>
                      <td class="text-right" class="text-right" colspan="2">PKR <strong class="subtotal"></strong></td>
                    </tr>

                    <tr>
                      <th colspan="2" class="discount_label">Discount:</th>
                      <td class="text-right" colspan="2">PKR <strong class="discount">0.00</strong></td>
                    </tr>

                    <tr>
                      <th colspan="2" class="tax_label">GST:</th>
                      <td class="text-right" colspan="2">PKR <strong class="tax">0.00</strong></td>
                    </tr>

                    <tr>
                      <th colspan="2">Grand Total:</th>
                      <td class="text-right" colspan="2">PKR <strong class="totalamount"></strong></td>
                    </tr>

                  </tbody>

                </table>

              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                <p>Thank you for your visit</p>

              </div>

              <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                <p>Have a nice day</p>

              </div>

              <hr>

              <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                <p>Powered by <strong> Arsalan Yousuf </strong>(+92 320 1251328)</p>

              </div>

            </div><!-- row 2 -->

          </div> 

        </div>

      </div>

      <div class="modal-footer">
        <a class="btn btn-info" id="print_out_this_order">Print</a>
      </div>
    </div>

  </div>
</div>