<style>
  .ui-autocomplete { z-index:2147483647; }
</style>

<div class="modal fade-scale" id="customerModal" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #696969;color: #FFF;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" style="font-size: 18px">Add Customer Details</h3>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="customer_details">
          <form action="">
            <div class="form-group">
              <label for="email">Customer Name *</label>
              <input type="text" class="form-control" id="name">
            </div>
            <div class="form-group">
              <label for="pwd">Contact No *</label>
              <input type="number" class="form-control" id="number">
            </div>
            <div class="form-group">
              <label for="pwd">Address *</label>
              <textarea class="form-control" rows="5" id="address"></textarea>
            </div>
            <button type="submit" data-dismiss="modal" class="btn btn-info">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div> <!-- Print Slip Modal End Here -->