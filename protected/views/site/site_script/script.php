<script type="text/javascript">
    $(document).ready(function (){
        setTimeout(function(){
            AutoFillOrder();
        },500);
    });

    // items autofilled after page refresh if session is not empty.
    function AutoFillOrder(){
        $.ajax({
            type: "POST",
            url: '<?php echo $this->createUrl('site/autoFillOrder'); ?>',
            data: {a: 1},
            success:function(msg){
                if(msg != 'empty'){
                    var Total = 0;
                    var subTotal = 0;
                    var item_count = 0;
                    var json_array = JSON.parse(msg);
                    for(var x in json_array){
                        getSingleProductHtml(json_array[x]);
                        getPrintSingleProductHtml(json_array[x]);
                        Total += parseInt(json_array[x]['total']);
                        subTotal += parseInt(json_array[x]['subtotal']);
                        item_count += parseInt(x.length);
                    }
                    getOverallCalculation(json_array, subTotal, Total, item_count);
                    $(".order").css("display","block");
                }
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    }


    $(function() {
        var selectedClass = "";
        $(".fil-cat").click(function(){ 
            selectedClass = $(this).attr("data-rel");
            $("#portfolio").fadeTo(100, 0.1);
            $("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
            setTimeout(function() {
                $("."+selectedClass).fadeIn().addClass('scale-anm');
                $("#portfolio").fadeTo(300, 1);
            }, 300); 

        });
    });


    $('body').on('click', '.add_product', function(){
        var product_id = $(this).attr('data-id');
        var cat_id = $(this).attr('data-cat-id');
        var name = $(this).attr('data-name');
        var price = $(this).attr('data-price');
        var size = $(this).attr('data-size');
        var image = $(this).attr('data-image');
        $.ajax({
            url: '<?php echo $this->createUrl('Site/AddSingle'); ?>',
            method: "post",
            dataType: "json",
            data: { product_id: product_id,
                cat_id: cat_id,
                name: name,
                size: size,
                price: price,
                image: image 
            },
            success: function(msg){
                $(".order-body").html('');
                $(".productSalePrint").html('');
                var Total = 0;
                var subTotal = 0;
                var item_count = 0;
                var json_array = JSON.parse(JSON.stringify(msg));
                for(var x in json_array){
                    getSingleProductHtml(json_array[x]);
                    getPrintSingleProductHtml(json_array[x]);
                    Total += parseInt(json_array[x]['total']);
                    subTotal += parseInt(json_array[x]['subtotal']);
                    item_count += parseInt(x.length);
                }
                getOverallCalculation(json_array, subTotal, Total, item_count);
                $(".order").css("display","block");
            },
            error: function(xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    });


    function getOverallCalculation(json, subtotal, total, items){
        $(".subtotal").html(addZeroAfterDecimel(0));
        $(".totalamount").html(addZeroAfterDecimel(0));
        $(".discount").html(addZeroAfterDecimel(0));
        $(".tax").html(addZeroAfterDecimel(0));
        $(".item-count").html(0);
        if(json && json.length){
            var overall_discount = 0;
            var overall_tax = 0;
            var overall_amount = 0;
            
            if(json[0]['discount']){
                if(json[0]['discount_type'] == "percentage"){
                    $(".discount_label").html("Discount "+json[0]['discount']+'%:');
                }else{
                    $(".discount_label").html("Discount "+json[0]['discount']+' PKR:');
                }
                overall_discount = calculateOverallDiscount(json, subtotal);
                $(".discount").html(addZeroAfterDecimel(overall_discount));
            }

            if(json[0]['tax']){
                $(".tax_label").html("Add Sales Tax "+json[0]['tax']+'%:');
                overall_tax = calculateOverallTax(json, subtotal);
                $(".tax").html(addZeroAfterDecimel(overall_tax));
            }

            overall_amount = calculateGrandTotalWithDiscountTax(subtotal, overall_discount, overall_tax);

            $(".subtotal").html(addZeroAfterDecimel(subtotal));
            $(".totalamount").html(addZeroAfterDecimel(overall_amount));
            $(".item-count").html(items);
        }
    }



    $('body').on('click', '.add_multi_product', function(){
        var product_id = $(this).attr('data-id');
        var cat_id = $(this).attr('data-cat-id');
        $.ajax({
            url: '<?php echo $this->createUrl('Site/AddMultiple'); ?>',
            method: "post",
            dataType: "json",
            data: { product_id: product_id,
                cat_id: cat_id },
                success: function(msg){

                    $("#remain_product").html('');

                    getMultiProductHtml(msg);
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
    });

    $("body").on('click','.btn-plus',function(){
        var current_row = $(this);
        var pro_id = current_row.closest('.input-group').find(".product_qty").attr('data-product-id');
        $.ajax({
            url: '<?php echo $this->createUrl('site/productPlus'); ?>',
            type: "POST",
            data: {pro_id:pro_id},
            success:function(data){
                $(".order-body").html('');
                $(".productSalePrint").html('');
                var Total = 0;
                var subTotal = 0;
                var item_count = 0;
                var json_array = JSON.parse(data);
                for(var x in json_array){
                    getSingleProductHtml(json_array[x]);
                    getPrintSingleProductHtml(json_array[x]);
                    Total += parseInt(json_array[x]['total']);
                    subTotal += parseInt(json_array[x]['subtotal']);
                    item_count += parseInt(x.length);
                }
                getOverallCalculation(json_array, subTotal, Total, item_count);
            },
        });
    });

    $("body").on('click','.btn-minus',function(){

        var current_row = $(this);
        var qty = current_row.closest('.input-group').find(".product_qty").val();
        var pro_id = current_row.closest('.input-group').find(".product_qty").attr('data-product-id');

        if(qty == 1){
            return;
        }

        $.ajax({
            url: '<?php echo $this->createUrl('site/productMinus'); ?>',
            type: "POST",
            data: {pro_id:pro_id},
            success:function(data){

                $(".order-body").html('');
                $(".productSalePrint").html('');

                var Total = 0;
                var subTotal = 0;
                var item_count = 0;

                var json_array = JSON.parse(data);

                for(var x in json_array){

                    getSingleProductHtml(json_array[x]);
                    getPrintSingleProductHtml(json_array[x]);
                    Total += parseInt(json_array[x]['total']);
                    subTotal += parseInt(json_array[x]['subtotal']);
                    item_count += parseInt(x.length);

                }

                getOverallCalculation(json_array, subTotal, Total, item_count);
            },

        });

    });

    $("body").on('click','.delete-per-row-cart',function(){ 

        var current_row = $(this);
        var pro_id = $(this).attr('data-product-id');

        $.ajax({
            url: '<?php echo $this->createUrl('site/productDelete'); ?>',
            type: "POST",
            data: {pro_id:pro_id},
            success:function(data){

                current_row.closest('.order-div').remove();

                $(".order-body").html('');
                $(".productSalePrint").html('');

                var Total = 0;
                var subTotal = 0;
                var item_count = 0;

                var json_array = JSON.parse(data);

                if(json_array && json_array.length > 0){
                    for(var x in json_array){
                        getSingleProductHtml(json_array[x]);
                        getPrintSingleProductHtml(json_array[x]);
                        Total += parseInt(json_array[x]['total']);
                        subTotal += parseInt(json_array[x]['subtotal']);
                        item_count += parseInt(x.length);
                    }
                    getOverallCalculation(json_array, subTotal, Total, item_count);
                }else{
                    getOverallCalculation(json_array, 0, 0, 0);
                }

            },

        });

    });

    function calculateOverallDiscount(json, subtotal){
        if(json && json.length){
            var discount = 0;
            if(json[0]['discount_type'] == "percentage"){
                discount = subtotal * (json[0]['discount'] / 100);
            }else{
                discount = json[0]['discount'];
            }
            return parseInt(discount);
        }
        
    }

    function calculateOverallTax(json, subtotal){
        if(json && json.length){
            var tax = 0;
            if(json[0]['tax']){
                tax = subtotal * (json[0]['tax'] / 100);
            }
            return parseInt(tax);
        }
    }
    
    function calculateGrandTotalWithDiscountTax(subtotal, overall_discount, overall_tax){

        var overall_amount = 0;

        if(overall_discount >= 0){
            overall_amount = (subtotal - overall_discount);
        }
        if(overall_tax >= 0){
            overall_amount = parseInt(overall_amount + overall_tax);
        }

        if(overall_amount > 0){
            return overall_amount;
        }else{
            return subtotal;
        }
    }

    function getPrintSingleProductHtml(json){

        var productDesign = '';

        productDesign += '<tr>';

        productDesign += '<td class="text-left">'+json.quantity+'</td>';

        if(json.size){
            productDesign += '<td>'+json.pname+' - '+json.size+'</td>';
        }
        else{
            productDesign += '<td>'+json.pname+'</td>';
        }

        productDesign += '<td class="text-right">'+json.price+'</td>';
        productDesign += '<td class="text-right">'+json.total+'</td>';

        productDesign += '</tr>';

        $(".productSalePrint").prepend(productDesign);

    }

    function getSingleProductHtml(json_array){

        var order_div = "";

        order_div += "<div class='container-fluid well order-div' style='margin-right:4px'>";
        order_div += "<div class='row'>";
        order_div += "<div class='col-md-12 col-xs-12'>";

        if(json_array.size){
            order_div += "<h4 class='product_name' data-product-id='"+json_array.product_id+"' data-cat-id='"+json_array.cat_id+"' data-price='"+json_array.price+"'>"+json_array.pname+" ("+json_array.size+")</h4>";
        }else{
            order_div += "<h4 class='product_name' data-product-id='"+json_array.product_id+"' data-cat-id='"+json_array.cat_id+"' data-price='"+json_array.price+"'>"+json_array.pname+"</h4>";
        }

        order_div += "<h5><span class='label label-default'>Price: "+json_array.price+"</span></h5>";
        order_div += "</div>";
        order_div += "</div>";

        order_div += "<div class='row'>";
        order_div += "<div class='col-md-8'>";
        order_div += "<div class='input-group pdng'>";

        order_div += "<span class='input-group-btn'>";
        order_div += "<button type='button' class='btn btn-danger btn-number btn-minus'>";
        order_div += "<span class='fa fa-minus'></span>";
        order_div += "</button>";
        order_div += "</span>";

        order_div += "<input style='min-width:100%' type='text' class='form-control product_qty' value='"+json_array.quantity+"' min='1' max='100' readonly='true' data-product-id='"+json_array.product_id+"'>";

        order_div += "<span class='input-group-btn'>";
        order_div += "<button type='button' class='btn btn-success btn-number btn-plus'>";
        order_div += "<span class='fa fa-plus'></span>";
        order_div += "</button>";
        order_div += "</span>";

        order_div += "</div>";
        order_div += "</div>";

        order_div += "<div class='col-md-4'>";
        order_div += "<div class='input-group pdng'>";

        order_div += "<button class='btn btn-danger btn-delete delete-per-row-cart' data-product-id='"+json_array.product_id+"'><i class='fa fa-trash-o' aria-hidden='true'></i></button>";
        order_div += "</div>";
        order_div += "</div>";
        order_div += "</div>";
        order_div += "</div>";

        $("#order_panel .order-body").prepend(order_div);
    }

    function getMultiProductHtml(msg){

        var json_out = JSON.parse(JSON.stringify(msg));

        var new_div = "";

        for(var i in json_out){

            var onerror = '<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg';

            new_div += "<div class='col-lg-3 col-md-3 col-sm-6 col-xs-6 product'>";
            new_div += "<a style='text-decoration:none;' href='javascript:void(0)' data-id='"+json_out[i].product_id+"' data-cat-id='"+json_out[i].product_id+"' data-name='"+json_out[i].name+"' data-size='"+json_out[i].size+"' data-price='"+json_out[i].price+"' class='add_product'>";
            new_div += "<h4>"+json_out[i].name+"</h4>";
            new_div += "<h5 style='color: #E74C3C;font-size:18px'>("+json_out[i].size+")</h5>";
            new_div += "<h6 style='color: #34495E;font-weight: 600'>Price : "+json_out[i].price+"</h6>";
            if(json_out[i].image){
                new_div += "<img src='<?php echo Yii::app()->request->baseUrl;?>/admin/images/products/"+json_out[i].image+"' class='thumbnail img-responsive' style='min-width: 100%; height: 210px'>"; 
            }else{
                new_div += "<img src='<?php echo Yii::app()->baseUrl; ?>/arsalan/img/product1.jpg' class='thumbnail img-responsive' style='min-width: 100%; height: 150px'>";
            }
            new_div += "</a>";
            new_div += "</div>";

            $("#remain_product").html(new_div);

        }

        $('#myModal').modal('show');

    }


    $('.advance_search').click(function(){

        $('.advance_search').val("");
        $('.search').click();
        $("#portfolio .search-div-here").html("");

    });

    $('.advance_search_btn').click(function(){

        var products = <?php echo $products; ?>;
        var searchedText = $('.advance_search').val();

        $("#portfolio .search-div-here").css("display","block");

        var div = "";

        for(var i = 0; i < products.length; i++){

            if (searchedText == products[i].name){
                if(products[i].size == 'Yes') {

                    div += "<div class='col-md-3 col-sm-3 col-xs-6 product scale-anm tooltips' title='"+products[i].name+"'>";

                    div += "<a href='javascript:void(0)' style='text-decoration:none' data-id='"+products[i].id+"' data-cat-id='"+products[i].cat_id+"' class='add_multi_product'>";

                    div += "<h4 style='font-size: 14px;font-weight: 600'>"+products[i].name+"</h4>";

                    div += "<h5><span class='label label-danger'>Price : "+products[i].price+"</span></h5>";

                    div += "<img src='<?php echo Yii::app()->request->baseUrl;?>/arsalan/img/product1.jpg' class='thumbnail img-responsive' style='min-width: 100%; height: 150px'>"; 

                    div += "</a>";

                    div += "</div>";

                }else{

                    div += "<div class='col-md-4 col-sm-4 col-xs-6 product scale-anm tooltips' title='"+products[i].name+"'>";

                    div += "<a href='javascript:void(0)' style='text-decoration:none' data-id='"+products[i].id+"' data-cat-id='"+products[i].cat_id+"' data-name='"+products[i].name+"' data-price='"+products[i].price+"' data-image='"+products[i].image+"' class='add_product'>";

                    div += "<h4 style='font-size: 14px;font-weight: 600'>"+products[i].name+"</h4>";

                    div += "<h5><span class='label label-danger'>Price : "+products[i].price+"</span></h5>";

                    div += "<img src='<?php echo Yii::app()->request->baseUrl;?>/arsalan/img/product1.jpg' class='thumbnail img-responsive' style='min-width: 100%; height: 210px'>"; 

                    div += "</a>";

                    div += "</div>";

                }          
                $("#portfolio .search-div-here").html(div);
            }
        }

    });

    $(document).ready(function($){

        $('.advance_search').autocomplete({
            source:"<?php echo Yii::app()->CreateUrl('Product/AutoCompProducts'); ?>", 
            minLength:1,
            select: function(event,ui){},
        });


        $('#name').autocomplete({
            source:"<?php echo Yii::app()->CreateUrl('Site/AutoCompCustomerDetails'); ?>", 
            minLength:1,
            select: function(event,ui){
                $("#number").val( ui.item.number );
                $("#address").val( ui.item.address );
            },
        });

    });


    function resetSession() {
        $.ajax({
            type: "POST",
            url:'<?php echo $this->createUrl("Site/ClearedSession"); ?>',
            data: "action=unsetsession",
            success: function(msg){
                console.log("Hello");
            },
            error: function(msg){
                alert('Error: cannot load page.');
            }
        });
    }


    $(document).ready(function() {

        $('.tooltips').tooltipster({
            theme: 'tooltipster-punk',
            animation: 'grow',
            delay: 200
        });

        $("#clear_btn").click(function(){
            $('#order_panel .order-body').html("");
            $('.pro_total').html("");
        });


        $(".dinein").click(function(){
            $(".table-waiter").css("display","block");
            $(".additional-information").css("display","none");
        });

        $(".takeaway").click(function(){
            $(".table-waiter").css("display","none");
            $(".additional-information").css("display","none");
        });

        $(".delivery").click(function(){
            $(".table-waiter").css("display","none");
            $(".additional-information").css("display","block");
        });


        <?php $myIp = getHostByName(getHostName()); ?>

        $('body').on('click', '#order_btn', function() {
            var order_type = $(this).closest("#products-listing").find("li.active").attr('order-type');
            var product_total = $(".totalamount").html();
            var subtotal = $(".subtotal").html();
            var waiter_id = $("#waiters").val();
            var table_id = $("#tables").val();
            var name = $("#name").val();
            var number = $("#number").val();
            var address = $("#address").val();
            var additional_information = $("#additional-information").val();
            var ipaddress = '<?php echo $myIp; ?>';

            if(order_type == "Dine in" && table_id == "") {
                toastem.setToastContent("Please Select Table");
                $("#error-alert").click();
                return false;
            }

            if(order_type == "Dine in" && waiter_id == "") {
                toastem.setToastContent("Please Select Waiter");
                $("#error-alert").click();
                return false;
            }

            var data = [];
            var error_str = "";
            var counter = 1;

            $("#side_panel .order-div").each(function(e){

                pname = $(this).find(".product_name").html();
                product_id = $(this).find(".product_name").attr('data-product-id');
                cat_id = $(this).find(".product_name").attr('data-cat-id');
                price = $(this).find(".product_name").attr('data-price');
                qty = $(this).find(".product_qty").val();

                if(qty.trim().length == 0){
                    return true;
                }

                var valueToPush = { };

                valueToPush["pname"] = pname;
                valueToPush["product_id"] = product_id;
                valueToPush["cat_id"] = cat_id;
                valueToPush["price"] = price;
                valueToPush["qty"] = qty;
                valueToPush["total"] = product_total;
                valueToPush["type"] = order_type;

                data.push(valueToPush);
            });

            if(data.length == 0){
                return true;
            }

            $.ajax({
                url:'<?php echo $this->createUrl("Checkout/OrderCheckout"); ?>',
                type:"post",
                data: {data: data,
                    order_type: order_type,
                    table_id: table_id,
                    waiter_id: waiter_id,
                    subtotal: subtotal,
                    product_total: product_total,
                    ipaddress: ipaddress,
                    name: name,
                    number: number,
                    address: address,
                    additional_information: additional_information },
                    success: function(msg){
                        if(msg == 'dine'){
                            $('#printModal').modal('show');
                            $('.table-name').text(table_id);
                            $(".order-type").text(order_type);
                        }
                        else if(msg == 'take'){
                            $('#printModal').modal('show');
                            $(".order-type").text(order_type);
                        }else{
                            $('#printModal').modal('show');
                            $(".order-type").text(order_type);
                        }
                    },
                    error: function(xhr, status, error) {
                        alert(xhr.responseText);
                    } ,
                });

        });

    });

$("#print_out_this_order").click(function(){

    <?php

    $data['hide_element_classes'] = "navbar,sidebar,menu-toggle2,container,modal,print_hide,container-fluid,main-content,footer";
    $data['printable_area_id'] = "printableArea";

    $this->renderPartial('application.components.danish.print_script',array(
        'data'=>$data,
    ));

    ?>

    resetSession();
    location.reload(true);

});

function addZeroAfterDecimel(num) {
    num = num.toFixed(2);
// Return the number
return num;
}
</script>



<script>
    window.onload = date_time('date_time');

    function date_time(id)
    {
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
            h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
            m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
            s = "0"+s;
        }
        result = ''+days[day]+', '+d+' '+months[month]+' '+year+', '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
    }
</script>