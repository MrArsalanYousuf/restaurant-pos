<?php

class CheckoutController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','OrderCheckout'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Checkout;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Checkout']))
		{
			$model->attributes=$_POST['Checkout'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Checkout']))
		{
			$model->attributes=$_POST['Checkout'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Checkout');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Checkout('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Checkout']))
			$model->attributes=$_GET['Checkout'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Checkout the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Checkout::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Checkout $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='checkout-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionOrderCheckout(){
		
		$sessions = $_POST['data'];

		$order_type = $_POST['order_type'];
		$table_id = $_POST['table_id'];
		$waiter_id = $_POST['waiter_id'];
		$name = trim($_POST['name']);
		$number = $_POST['number'];
		$addr = trim($_POST['address']);
		$subtotal = $_POST['subtotal'];
		$total = $_POST['product_total'];
		$ipaddress = $_POST['ipaddress'];
		$additional_information = $_POST['additional_information'];
		$date = date('Y-m-d H:i:s');
		
		print_r($date);

		$model = new Checkout;
		$model->order_type = $order_type;
		$model->subtotal = $subtotal;
		$model->order_status = 0;
		$model->tax = 0;
		$model->discount = 0;
		$model->additional_information = $additional_information;
		$model->net_amount = 0;
		$model->total = $total;
		$model->ip = $ipaddress;
		$model->user_id = (Yii::app()->user->id ? Yii::app()->user->id : 0);
		$model->table_id = ($table_id ? $table_id : "");
		$model->date_added = $date;
		$model->date_modified = $date;
		$model->save(false);

		$receipt_no = "000".$model->id;

		Checkout::model()->updateAll(array('receipt_no'=>$receipt_no),"id = $model->id");

		if($order_type == "Dine in"){

			$Waiterorder = new Waiterorder;
			$Waiterorder->waiter_id = $waiter_id;
			$Waiterorder->checkout_id = $model->id;
			$Waiterorder->table_id = $table_id;
			$Waiterorder->date_added = $date;
			$Waiterorder->save(false);

		}


		if($order_type == "Delivery"){

			$address = new Address;
			$address->name = $name;
			$address->number = $number;
			$address->address = trim($addr);
			$address->checkout_id = $model->id;
			$address->created_by = (Yii::app()->user->id ? Yii::app()->user->id : 0);
			$address->date_added = $date;

			$address->save(false);

		}

		$save = "";

		foreach($sessions as $session){
			
			$OrderProduct = new OrderProduct;
			$OrderProduct->product_id = $session['product_id'];
			$OrderProduct->cat_id = $session['cat_id'];
			$OrderProduct->total = $session['total'];
			$OrderProduct->checkout_id = $model->id;
			$OrderProduct->name = $session['pname'];
			$OrderProduct->price = $session['price'];
			$OrderProduct->quantity = $session['qty'];
			$OrderProduct->tax = 0;
			$OrderProduct->order_status = 0;
			$OrderProduct->date_added = $date;
			$OrderProduct->date_modified = $date;

			if($OrderProduct->save(false)){
			 	$save = "true";
			}else{
			 	$save = "false";
			}
		}

		//unset($_SESSION['selected_product']);

			if($save == 'true'){

				if($order_type == 'Dine in'){
					echo "dine";
				}
				elseif($order_type == 'Takeaway'){
					echo "take";
				}else{
					echo "delivery";
				}

			}else{
				echo "fail";
			}
	}
}
