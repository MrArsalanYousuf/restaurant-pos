<?php

session_start();

class SiteController extends Controller
{
	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		
		if(Yii::app()->user->isGuest){
			$this->redirect(array('site/login'));
		}


		$cats = Category::model()->findAll(array("condition"=>"status = 1 ORDER BY priority ASC"));
		$mp = Product::model()->findAll(array("condition"=>"status = 1 ORDER BY name ASC"));
		$waiters = Users::model()->findAll(array("condition"=>"user_type='waiter' AND status = 1 ORDER BY username ASC"));
		$tables = TableFloor::model()->findAll(array("condition"=>"t_status = 1 ORDER BY name ASC"));

		if(!empty($mp)){

			$temp = array();
			$temp1 = array();

			foreach($mp as $product){
				$temp1['id'] = $product->id;
				$temp1['cat_id'] = $product->cat_id;
				$temp1['name'] = $product->name;
				$temp1['price'] = $product->price;
				$temp1['image'] = $product->image;
				$temp1['size'] = $product->size;
				$temp[] = $temp1;
			}

			$products = json_encode($temp);

		}
		
		$this->render('index',array(
			'cats'=>$cats,
			'mp'=>$mp,
			'products'=>$products,
			'waiters'=>$waiters,
			'tables'=>$tables
		)
	);
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
				"Reply-To: {$model->email}\r\n".
				"MIME-Version: 1.0\r\n".
				"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];

			Yii::app()->session['logged_in_user'] = isset($_POST['LoginForm']['username'])? $_POST['LoginForm']['username']:'';
			Yii::app()->session['logged_in_user'] = isset($_POST['LoginForm']['password'])? $_POST['LoginForm']['password']:'';


			if($model->validate() && $model->login()){
				// For Mobile Application Only

				if (isset($_POST['tourist_key'])) {
					if ($_POST['tourist_key'] == 'yest') {
						$json = array();
						$row = Yii::app()->session['logged_in_user'];

						$json['user']['user_id'] = $row->api_key;
						$json['user']['f_name'] = $row->f_name;
						$json['user']['l_name'] = $row->l_name;

						echo json_encode($json);
						die();
					}
				}

				$this->redirect(Yii::app()->user->returnUrl);
			}
			else{

			}
		}

		// display the login form
		$this->renderPartial('login_now',array('model'=>$model));
	}
	

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('login'));
	}

	public function actionLoginCheck()
	{
		$model=new LoginForm;
		
		if(Yii::app()->request->isAjaxRequest) 
		{
			
			$model->attributes=$_POST;
			if($model->validate() && $model->login()){

				echo "Success";
			}else{

				echo Yii::app()->session['Error'] ;

			}
		}
	}

	public function actionAddSingle(){

		$product_id = $_POST['product_id'];
		$cat_id = $_POST['cat_id'];
		$name = $_POST['name'];
		$size = $_POST['size'];
		$price = $_POST['price'];
		$image = $_POST['image'];
		$subtotal = $price;
		$total = $price;
		$quantity = 1;
		$path = Yii::app()->baseUrl.'/admin/images/products/';

		$accessories = Accessories::model()->find();

		if(!empty($_SESSION['selected_product'])){

			foreach($_SESSION["selected_product"] as $k => $v) {

				if($v['product_id'] == $product_id){

					$_SESSION['selected_product'][$k]['quantity'] += $quantity;
					$_SESSION['selected_product'][$k]['subtotal'] += $subtotal;
					$_SESSION['selected_product'][$k]['total'] += $total;

					$indexed_session = array_values($_SESSION['selected_product']);
					echo json_encode($indexed_session);die();
				}

			}
		}

		$temp = array();
		$temp1 = array();

		$temp['product_id'] = $product_id;
		$temp['cat_id'] = $cat_id;
		$temp['pname'] = $name;
		$temp['size'] = $size;
		$temp['price'] = $price;
		$temp['discount'] = !empty($accessories->discount) ? (int)$accessories->discount : 0;
		$temp['discount_type'] = !empty($accessories->type) ? $accessories->type : NULL;
		$temp['tax'] = !empty($accessories->tax) ? (int)$accessories->tax : 0;
		$temp['subtotal'] = $subtotal;
		$temp['total'] = $total;
		$temp['quantity'] = $quantity;
		$temp['image'] = $path.$image;

		$temp1[] = $temp;

		if(empty($_SESSION['selected_product'])){
			$_SESSION['selected_product'] = $temp1;
		}else{
			$_SESSION["selected_product"] = array_merge($_SESSION["selected_product"],$temp1);

			$indexed_session = array_values($_SESSION['selected_product']);
			echo json_encode($indexed_session);die();
		}

        $indexed_session = array_values($_SESSION['selected_product']);
		echo json_encode($indexed_session);die();
	}


	public function actionAddMultiple(){

		$product_id = $_POST['product_id'];
		$cat_id = $_POST['cat_id'];

		$op = Product::model()->findAll(array('condition'=>"product_id = $product_id AND cat_id = $cat_id AND status = 2"));

		$temp = array();
		$temp1 = array();

		foreach ($op as $k => $v) {
			$temp['product_id'] = $v->id;
			$temp['cat_id'] = $v->cat_id;
			$temp['name'] = $v->name;
			$temp['price'] = $v->price;
			$temp['size'] = $v->size;
			$temp['image'] = $v->image;

			$temp1[] = $temp;
		}

		echo json_encode($temp1);die();
	}

	public function actionClearedSession(){

		if($_POST['action'] == "unsetsession"){
			unset($_SESSION['selected_product']);
			echo "success";
		}

	}

	public function actionproductPlus(){

		$id = (int) $_POST['pro_id'];

		if(isset($_SESSION['selected_product'])){

			$products = $_SESSION['selected_product'];

			foreach($products as $k => $product){

				if($product['product_id'] == $id){

					$_SESSION['selected_product'][$k]['quantity'] += 1;
					$_SESSION['selected_product'][$k]['subtotal'] += $product['price'];
					$_SESSION['selected_product'][$k]['total'] += $product['price'];
	                
	                $indexed_session = array_values($_SESSION['selected_product']);
					echo json_encode($indexed_session);die();
				}
			}

		}else{
			echo "empty";die();
		}
	}


	public function actionproductMinus(){

		$id = (int) $_POST['pro_id'];

		if(isset($_SESSION['selected_product'])){

			$products = $_SESSION['selected_product'];

			foreach($products as $k => $product){

				if($product['product_id'] == $id){

					$_SESSION['selected_product'][$k]['quantity'] -= 1;
					$_SESSION['selected_product'][$k]['subtotal'] -= $product['price'];
					$_SESSION['selected_product'][$k]['total'] -= $product['price'];
                    
                    $indexed_session = array_values($_SESSION['selected_product']);
					echo json_encode($indexed_session);die();
				}
			}

		}else{
			echo "empty";die();
		}

	}

	public function actionproductDelete(){

		$id = (int) $_POST['pro_id'];

		if(isset($_SESSION['selected_product'])){

			$products = $_SESSION['selected_product'];

			foreach($products as $key => $product){

				if($product['product_id'] == $id){

					unset($_SESSION['selected_product'][$key]);

					$indexed_session = array_values($_SESSION['selected_product']);
					echo json_encode($indexed_session);die();
				}
			}

		}else{
			echo "empty";die();
		}

	}

	public function actionautoFillOrder(){
		if(isset($_SESSION['selected_product']) && !empty($_SESSION['selected_product'])){
            $indexed_session = array_values($_SESSION['selected_product']);
			echo json_encode($indexed_session);die();
		}else{
			echo "empty";die();
		}
	}

	public function actionAutoCompCustomerDetails(){
		
		$searched_word = $_GET['term'];
		
		$match = addcslashes($searched_word, '%_'); // escape LIKE's special characters\

		$q = new CDbCriteria( array(
				'condition' => "name LIKE :match",         // no quotes around :match
				'params'    => array(':match' => "%$match%")  // Aha! Wildcards go here
			) );
		
		$rows = Address::model()->findAll($q);

		$json_data = array();
		$arr = array();

		foreach($rows as $row){

			$arr['label'] = htmlentities(stripslashes($row->name));
			$arr['number'] = $row->number;
			$arr['address'] = htmlentities(stripslashes($row->address));
			$arr['id'] = (int)$row->id;
			$json_data[] = $arr;

		}

		echo json_encode($json_data);//format the array into json data
	}
}