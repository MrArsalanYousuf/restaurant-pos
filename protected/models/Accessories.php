<?php

/**
 * This is the model class for table "accessories".
 *
 * The followings are the available columns in table 'accessories':
 * @property integer $id
 * @property string $logo
 * @property string $favi_image
 * @property string $title
 * @property string $footer_name
 * @property string $date_added
 */
class Accessories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'accessories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('logo, title, footer_name, date_added', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('title, footer_name,email,phone_no,address', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, logo, favi_image,email, title, footer_name, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'logo' => 'Logo',
			'favi_image' => 'Favi Image',
			'title' => 'Title',
                        'email' => 'Email',
						'phone_no' => 'Phone #',
			'footer_name' => 'Footer Name',
			'date_added' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phone_no',$this->phone_no);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('favi_image',$this->favi_image,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('footer_name',$this->footer_name,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Accessories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
