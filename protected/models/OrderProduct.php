<?php

/**
 * This is the model class for table "order_product".
 *
 * The followings are the available columns in table 'order_product':
 * @property string $id
 * @property string $product_id
 * @property integer $cat_id
 * @property double $total
 * @property integer $order_status
 * @property double $price
 * @property string $name
 * @property double $tax
 * @property integer $quantity
 * @property integer $table_id
 * @property string $checkout_id
 * @property string $date_added
 * @property string $date_modified
 */
class OrderProduct extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, cat_id, total, price, name, quantity, table_id, date_added, date_modified', 'required'),
			array('cat_id, order_status, quantity, table_id, waiter_id', 'numerical', 'integerOnly'=>true),
			array('total, price, tax', 'numerical'),
			array('product_id, checkout_id', 'length', 'max'=>10),
			array('name', 'length', 'max'=>256),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, cat_id, total, order_status, price, name, tax, quantity, table_id, checkout_id, waiter_id, date_added, date_modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'cat_id' => 'Cat',
			'total' => 'Total',
			'order_status' => 'Order Status',
			'price' => 'Price',
			'name' => 'Name',
			'tax' => 'Tax',
			'quantity' => 'Quantity',
			'table_id' => 'Table',
			'checkout_id' => 'Checkout',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('total',$this->total);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('price',$this->price);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('table_id',$this->table_id);
		$criteria->compare('checkout_id',$this->checkout_id,true);
		$criteria->compare('waiter_id',$this->waiter_id,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
