<?php

/**
 * This is the model class for table "table_floor".
 *
 * The followings are the available columns in table 'table_floor':
 * @property string $id
 * @property string $code
 * @property string $description
 * @property string $name
 * @property integer $t_status
 * @property string $user_id
 * @property integer $no_of_seats
 * @property string $plan_id
 * @property string $image
 * @property string $date_added
 */
class TableFloor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'table_floor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, t_status, user_id, plan_id, image, date_added', 'required'),
			array('t_status, no_of_seats', 'numerical', 'integerOnly'=>true),
			array('code, name', 'length', 'max'=>256),
			array('user_id', 'length', 'max'=>11),
			array('plan_id', 'length', 'max'=>10),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, description, name, t_status, user_id, no_of_seats, plan_id, image, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Code',
			'description' => 'Description',
			'name' => 'Name',
			't_status' => 'T Status',
			'user_id' => 'User',
			'no_of_seats' => 'No Of Seats',
			'plan_id' => 'Plan',
			'image' => 'Image',
			'date_added' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('t_status',$this->t_status);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('no_of_seats',$this->no_of_seats);
		$criteria->compare('plan_id',$this->plan_id,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TableFloor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
