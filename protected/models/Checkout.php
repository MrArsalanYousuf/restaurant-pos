<?php

/**
 * This is the model class for table "checkout".
 *
 * The followings are the available columns in table 'checkout':
 * @property string $id
 * @property string $order_type
 * @property integer $order_status
 * @property double $tax
 * @property double $discount
 * @property double $total
 * @property double $net_amount
 * @property string $products
 * @property string $ip
 * @property string $user_id
 * @property string $date_added
 * @property string $date_modified
 */
class Checkout extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'checkout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_type, receipt_no, order_status, tax, discount, total, ip, user_id, date_added, date_modified', 'required'),
			array('order_status, user_id', 'numerical', 'integerOnly'=>true),
			array('subtotal, tax, discount, total, net_amount', 'numerical'),
			array('ip, order_type, receipt_no', 'length', 'max'=>50),
			array('products, additional_information', 'safe'),
			array('id, order_type, order_status, subtotal, tax, discount, total, net_amount, products, ip, user_id, date_added, date_modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_type' => 'Order Type',
			'order_status' => 'Order Status',
			'tax' => 'Tax',
			'discount' => 'Discount',
			'total' => 'Total',
			'net_amount' => 'Net Amount',
			'products' => 'Products',
			'additional_information' => 'Additional Information',
			'ip' => 'Ip',
			'user_id' => 'User',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('order_type',$this->order_type,true);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('tax',$this->tax);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('total',$this->total);
		$criteria->compare('net_amount',$this->net_amount);
		$criteria->compare('products',$this->products,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Checkout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
