var toastem = (function($){
	 var newContent = "";
 // var normal = function(content){
	 var setToastContent = function(content){
		 newContent = content;
		 
	 };
  $("#normal-alert").on('click', function(){
    var item = $('<div class="notification normal"><span class="fa fa-check"> '+newContent+'</span></div>');
    $("#toastem").append($(item));
    $(item).animate({"right":"12px"}, "fast");
    setInterval(function(){
      $(item).animate({"right":"-400px"},function(){
        $(item).remove();
      });
    },4000);
  });
//};



//var success = function(content){
	
  $("#success-alert").on('click', function(){
      var item = $('<div class="notification success"><span>'+newContent+'</span></div>');
      $("#toastem").append($(item));
      $(item).animate({"right":"12px"}, "fast");
      setInterval(function(){
        $(item).animate({"right":"-400px"},function(){
          $(item).remove();
        });
      },4000);
    });
//};


//var error = function(content){
  $("#error-alert").on('click', function(){
    var item = $('<div class="notification error"><span class="fa fa-remove"> '+newContent+'</span></div>');
    $("#toastem").append($(item));
    $(item).animate({"right":"12px"}, "fast");
    setInterval(function(){
      $(item).animate({"right":"-400px"},function(){
        $(item).remove();
      });
    },4000);
  });
//};

  $(document).on('click','.notification', function(){
      $(this).fadeOut(400,function(){
        $(this).remove();
      });
  });

  return{
    setToastContent: setToastContent
    
  };

})(jQuery);
