<?php
/* @var $this SizeController */
/* @var $model Size */

$this->breadcrumbs=array(
	'Sizes'=>array('index'),
	'Create',
);

?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>