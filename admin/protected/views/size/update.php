<?php
/* @var $this SizeController */
/* @var $model Size */

$this->breadcrumbs=array(
	'Sizes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>