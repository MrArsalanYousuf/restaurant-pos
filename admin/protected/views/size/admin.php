<?php

$this->breadcrumbs=array(
	'Sizes'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#size-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<br>

<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sizes Information</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>

                        <th scope="col" style="width: 500px !important">
							Name
						</th>

						<th scope="col" style="width: 300px !important">
							Action
						</th>

                    </tr>
                  </thead>
                  <tbody>
                      			<?php $this->widget('zii.widgets.CListView', array(
									'dataProvider'=>$model->search(),
									'itemView'=>'_view',
								)); ?>
                  </tbody>

                    </table>
                    </div>

                    			<div class='div-pagination Personal_info'></div>
								<style>
								.page_info{
									display:none !important;
								}
								.div-pagination.Personal_info{
									text-align:center;
								}
								.yiiPager>li>a {
									    position: relative;
   
										padding: 6px 12px !important;
										margin-left: -1px !important;
										line-height: 1.42857143 !important;
										color: #3b9c96 !important;
										text-decoration: none !important;
										background-color: #fff !important;
										border: 1px solid #ddd !important;
										border-radius: 2px !important;
								}
								ul.yiiPager .selected a {
									color:#fff !important;
									background-color:#3b9c96 !important;
								}
								.pager .next>a, .pager .next>span {
									float:none;
								}
								/*ul.yiiPager .first, ul.yiiPager .last {
									display: block !important;
								}*/
								.pager .previous>a, .pager .previous>span {
									float:none;
								}
								.pager {
									font-size:0px;
								}
								
								</style>

                  </div>
                </div>
              </div>
             </div>
           </div>