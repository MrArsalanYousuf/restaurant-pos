<?php 
date_default_timezone_set("Asia/Karachi");
$current_day = date('Y-m-d');
$current_month = date('m');

$per_day_orders = Checkout::model()->count(array("condition"=>"DATE(`date_added`) = '$current_day'"));
$per_month_orders = Checkout::model()->count(array("condition"=>"MONTH(`date_added`) = '$current_month'"));
$recent_checkouts = Checkout::model()->findAll(array('order'=>'id DESC LIMIT 10'));

$select_query = "SELECT SUM(total) AS totalamountperday from checkout where DATE(`date_added`) = '$current_day'";
$results = Yii::app()->db->createCommand($select_query)->queryAll();
$total_per_day = $results[0]['totalamountperday'];

$select_query = "SELECT SUM(total) AS totalamountpermonth from checkout where MONTH(`date_added`) = '$current_month'";
$results = Yii::app()->db->createCommand($select_query)->queryAll();
$total_per_month = $results[0]['totalamountpermonth'];
?>


<div class="row tile_count">
  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
      <a href="javascript:void(0)" target="_blank"><span class="count_top">Total Revenue Per Day</span></a>
      <div class="count"><?php echo ($total_per_day ? $total_per_day : 0); ?></div>
    </div>

    <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
      <a href="javascript:void(0)" target="_blank"><span class="count_top">Total Revenue Per Week</span></a>
      <div class="count"><?php echo ($total_per_month ? $total_per_month : 0); ?></div>
    </div>

    <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
      <a href="javascript:void(0)" target="_blank"><span class="count_top">Total Revenue Per Month</span></a>
      <div class="count"><?php echo ($total_per_month ? $total_per_month : 0); ?></div>
    </div>


    <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
      <a href="<?php echo Yii::app()->createUrl('Checkout/admin'); ?>" target="_blank"><span class="count_top">Orders</span></a>
      <div class="count"><?php echo $per_day_orders; ?></div>
    </div>

    <div class="col-md-2 col-sm-12 col-xs-12 tile_stats_count">
      <a href="<?php echo Yii::app()->createUrl('Checkout/admin'); ?>" target="_blank"><span class="count_top">Monthly Orders</span></a>
      <div class="count"><?php echo $per_month_orders; ?></div>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-md-8 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title" style="margin-bottom: 20px !important">
        <h1>Recent Orders</h1>
        <ul class="nav navbar-right panel_toolbox">
          <a href="<?php echo Yii::app()->CreateUrl("Checkout/Admin/"); ?>" class="btn btn-primary">View All</a>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive" style="height: 500px">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>S#</th>
                <th>Date & Time</th>
                <th>Receipt No.</th>
                <th>Type</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($recent_checkouts as $key => $value){ 
                $orders = OrderProduct::model()->findAll(array("condition"=>"checkout_id = $value->id"));
                ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <td><?php echo date('l, d F Y h:i A', strtotime($value->date_added)); ?></td>
                  <td style="color: blue !important; font-weight: 600"><?php echo $value->receipt_no; ?></td>
                  <td><?php echo $value->order_type; ?></td>
                  <td>
                    <a href="<?php echo Yii::app()->CreateUrl("Checkout/Update/".$value->id); ?>" class="btn btn-success fa fa-pencil"></a>
                    <!-- <a href="<?php //echo Yii::app()->CreateUrl("Checkout/View/".$value->id); ?>" class="btn btn-info fa fa-eye"></a> -->
                    <a href="<?php echo Yii::app()->CreateUrl("Checkout/Slip/".$value->id); ?>" class="btn btn-warning fa fa-print"></a>
                  </td>
                </tr>
              <?php } ?>

            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Orders Summary</h1>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <div id="order_summary_pyramid_graph" style="min-height:500px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-8 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Daily Sales Revenue</h1>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <div id="daily_sales_revenue_graph" style="height: 500px;"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Daily Sales Revenue</h1>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <div id="daily_sales_revenue_donut_graph" style="height: 500px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Monthly Sales Revenue</h1>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="table-responsive">
          <div id="monthly_sales_revenue_graph" style="height: 500px;"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h1>Most Popular Products</h1>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="table-responsive">
          <div id="most_popular_products" style="height: 500px;"></div>
        </div>

      </div>
    </div>
  </div>
</div>       
<?php include('pages/main.php');
include('pages/daily_sales_donut_graph.php');
include('pages/daily_sales_graph.php');
include('pages/weekly_order_pyramid_graph.php');
include('pages/monthly_sales_graph.php');
include('pages/popular_items_graph.php'); ?>