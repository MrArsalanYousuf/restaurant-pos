<script type="text/javascript">

    var products = '<?php echo $products; ?>';
    var products_data = '<?php echo $products_data; ?>';

    parse_products = JSON.parse(products);
    parse_products_data = JSON.parse(products_data);
    var current_day = '<?php echo date('F Y', strtotime($current_day)); ?>';

    var myChart = echarts.init(document.getElementById('most_popular_products'), theme);
    myChart.setOption({
        title: {
            text: 'Sales Revenue (PKR)',
            subtext: current_day
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            x: 'center',
            y: 'bottom',
            data: parse_products
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {
                    show: true
                }
            }
        },
        calculable: true,
        series: [{
            name: '',
            type: 'pie',
            radius: '55%',
            center: ['50%', '48%'],
            data: parse_products_data
        }]
    });
</script>