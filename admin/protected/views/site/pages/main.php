<?php

$current_year = date('Y');
$current_month = date('m');
$current_day = date('Y-m-d');
$previous_one_day = date('Y-m-d',(strtotime('-1 day',strtotime($current_day))));
$previous_two_day = date('Y-m-d',(strtotime('-2 day',strtotime($current_day))));
$previous_three_day = date('Y-m-d',(strtotime('-3 day',strtotime($current_day))));
$previous_four_day = date('Y-m-d',(strtotime('-4 day',strtotime($current_day))));
$previous_five_day = date('Y-m-d',(strtotime('-5 day',strtotime($current_day))));
$previous_six_day = date('Y-m-d',(strtotime('-6 day',strtotime($current_day))));
$previous_week = date('Y-m-d', strtotime("-1 week"));
$connection = Yii::app()->db;

// Dine In
$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 01 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$jan_res = $command->queryAll();
$jan_current_dine = $jan_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 02 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$feb_res = $command->queryAll();
$feb_current_dine = $feb_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 03 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$mar_res = $command->queryAll();
$mar_current_dine = $mar_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 04 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$apr_res = $command->queryAll();
$apr_current_dine = $apr_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 05 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$may_res = $command->queryAll();
$may_current_dine = $may_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 06 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$june_res = $command->queryAll();
$june_current_dine = $june_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 07 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$july_res = $command->queryAll();
$july_current_dine = $july_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 08 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$aug_res = $command->queryAll();
$aug_current_dine = $aug_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 09 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$sep_res = $command->queryAll();
$sep_current_dine = $sep_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 10 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$oct_res = $command->queryAll();
$oct_current_dine = $oct_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 11 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$nov_res = $command->queryAll();
$nov_current_dine = $nov_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 12 AND order_type = 'Dine in'";
$command  = $connection->createCommand($sql);
$dec_res = $command->queryAll();
$dec_current_dine = $dec_res[0]['total'];

// Take Away
$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 01 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$jan_res = $command->queryAll();
$jan_current_takeaway = $jan_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 02 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$feb_res = $command->queryAll();
$feb_current_takeaway = $feb_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 03 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$mar_res = $command->queryAll();
$mar_current_takeaway = $mar_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 04 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$apr_res = $command->queryAll();
$apr_current_takeaway = $apr_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 05 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$may_res = $command->queryAll();
$may_current_takeaway = $may_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 06 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$june_res = $command->queryAll();
$june_current_takeaway = $june_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 07 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$july_res = $command->queryAll();
$july_current_takeaway = $july_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 08 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$aug_res = $command->queryAll();
$aug_current_takeaway = $aug_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 09 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$sep_res = $command->queryAll();
$sep_current_takeaway = $sep_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 10 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$oct_res = $command->queryAll();
$oct_current_takeaway = $oct_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 11 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$nov_res = $command->queryAll();
$nov_current_takeaway = $nov_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 12 AND order_type = 'Takeaway'";
$command  = $connection->createCommand($sql);
$dec_res = $command->queryAll();
$dec_current_takeaway = $dec_res[0]['total'];


// Delivery
$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 01 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$jan_res = $command->queryAll();
$jan_current_delivery = $jan_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 02 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$feb_res = $command->queryAll();
$feb_current_delivery = $feb_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 03 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$mar_res = $command->queryAll();
$mar_current_delivery = $mar_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 04 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$apr_res = $command->queryAll();
$apr_current_delivery = $apr_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 05 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$may_res = $command->queryAll();
$may_current_delivery = $may_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 06 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$june_res = $command->queryAll();
$june_current_delivery = $june_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 07 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$july_res = $command->queryAll();
$july_current_delivery = $july_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 08 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$aug_res = $command->queryAll();
$aug_current_delivery = $aug_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 09 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$sep_res = $command->queryAll();
$sep_current_delivery = $sep_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 10 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$oct_res = $command->queryAll();
$oct_current_delivery = $oct_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 11 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$nov_res = $command->queryAll();
$nov_current_delivery = $nov_res[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE YEAR(`date_added`) = $current_year AND MONTH(`date_added`) = 12 AND order_type = 'Delivery'";
$command  = $connection->createCommand($sql);
$dec_res = $command->queryAll();
$dec_current_delivery = $dec_res[0]['total'];

// Most Wanted Products Monthly
$sql = "SELECT SUM(quantity) AS value,name FROM `order_product` WHERE MONTH(`date_added`) = '$current_month' AND YEAR(`date_added`) = '$current_year' group by product_id order by value DESC limit 10";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();

$temp = array();
$temp1 = array();
$temp2 = array();

foreach ($result as $value){

    $temp1['value'] = (int)$value['value'];
    $temp1['name'] = $value['name']." (".$value['value'].")";

    $temp[] = $value['name']." (".$value['value'].")";
    $temp2[] = $temp1;

}

$products = json_encode($temp);
$products_data = json_encode($temp2);

// Weekly Orders Summary Start
$sql = "SELECT COUNT(*) AS value, DATE_FORMAT(date_added, '%Y-%m-%d') AS date FROM `checkout` WHERE YEAR(`date_added`) = '$current_year' group by date order by date DESC limit 10";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();

$temp3 = array();
$temp4 = array();
$temp5 = array();

foreach ($result as $value){
    $temp4['value'] = $value['value'];
    $temp4['name'] = date('M, d, D', strtotime($value['date']))." (".$value['value'].")";

    $temp3[] = date('M, d, D', strtotime($value['date']))." (".$value['value'].")";
    $temp5[] = $temp4;
}

$pyramid1 = json_encode($temp3);
$pyramid2 = json_encode($temp5);
// Weekly Orders Summary End

// Daily Sales Revenue Donut
$sql = "SELECT SUM(total) AS value,order_type AS name FROM `checkout` WHERE DATE(`date_added`) = '$current_day' group by order_type order by value DESC";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$donut_graph = json_encode($result);

// Daily Sales Revenue Line Graph
$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$current_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$current_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_one_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_one_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_two_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_two_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_three_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_three_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_four_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_four_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_five_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_five_day_dine = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Dine in' AND DATE(`date_added`) = '$previous_six_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_six_day_dine = $result[0]['total'];


$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$current_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$current_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_one_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_one_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_two_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_two_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_three_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_three_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_four_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_four_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_five_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_five_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Takeaway' AND DATE(`date_added`) = '$previous_six_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_six_day_takeaway = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$current_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$current_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_one_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_one_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_two_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_two_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_three_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_three_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_four_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_four_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_five_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_five_day_delivery = $result[0]['total'];

$sql = "SELECT SUM(total) AS total FROM `checkout` WHERE order_type = 'Delivery' AND DATE(`date_added`) = '$previous_six_day'";
$command  = $connection->createCommand($sql);
$result = $command->queryAll();
$previous_six_day_delivery = $result[0]['total'];

$daily_sales_revenue_days = json_encode([date('M, d, D', strtotime($previous_six_day)), date('M, d, D', strtotime($previous_five_day)), date('M, d, D', strtotime($previous_four_day)), date('M, d, D', strtotime($previous_three_day)), date('M, d, D', strtotime($previous_two_day)), date('M, d, D', strtotime($previous_one_day)), date('M, d, D', strtotime($current_day))]);
?>