<script type="text/javascript">

  var pyramid1 = '<?php echo $pyramid1; ?>';
  var pyramid2 = '<?php echo $pyramid2; ?>';

  json_pyramid1 = JSON.parse(pyramid1);
  json_pyramid2 = JSON.parse(pyramid2);

  if ($("#order_summary_pyramid_graph").length) {
    var d = echarts.init(document.getElementById("order_summary_pyramid_graph"), theme);
    d.setOption({
      title: {
        text: "10 Days Order Summary",
        subtext: ""
      },
      tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} Orders"
      },
      toolbox: {
        show: !0,
        feature: {
          restore: {
            show: !0,
            title: "Restore"
          },
          saveAsImage: {
            show: !0,
            title: "Save Image"
          }
        }
      },
      legend: {
        data: json_pyramid1,
        orient: "vertical",
        x: "left",
        y: "bottom"
      },
      calculable: !0,
      series: [{
        name: "Order Summary",
        type: "funnel",
        width: "50%",
        data: json_pyramid2
      }]
    })
  }
</script>