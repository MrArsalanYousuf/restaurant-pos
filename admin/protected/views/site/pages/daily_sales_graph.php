<script type="text/javascript">

    var current_day_dine = "<?php echo $current_day_dine; ?>";
    var previous_one_day_dine = "<?php echo $previous_one_day_dine; ?>";
    var previous_two_day_dine = "<?php echo $previous_two_day_dine; ?>";
    var previous_three_day_dine = "<?php echo $previous_three_day_dine; ?>";
    var previous_four_day_dine = "<?php echo $previous_four_day_dine; ?>";
    var previous_five_day_dine = "<?php echo $previous_five_day_dine; ?>";
    var previous_six_day_dine = "<?php echo $previous_six_day_dine; ?>";

    var current_day_takeaway = "<?php echo $current_day_takeaway; ?>";
    var previous_one_day_takeaway = "<?php echo $previous_one_day_takeaway; ?>";
    var previous_two_day_takeaway = "<?php echo $previous_two_day_takeaway; ?>";
    var previous_three_day_takeaway = "<?php echo $previous_three_day_takeaway; ?>";
    var previous_four_day_takeaway = "<?php echo $previous_four_day_takeaway; ?>";
    var previous_five_day_takeaway = "<?php echo $previous_five_day_takeaway; ?>";
    var previous_six_day_takeaway = "<?php echo $previous_six_day_takeaway; ?>";

    var current_day_delivery = "<?php echo $current_day_delivery; ?>";
    var previous_one_day_delivery = "<?php echo $previous_one_day_delivery; ?>";
    var previous_two_day_delivery = "<?php echo $previous_two_day_delivery; ?>";
    var previous_three_day_delivery = "<?php echo $previous_three_day_delivery; ?>";
    var previous_four_day_delivery = "<?php echo $previous_four_day_delivery; ?>";
    var previous_five_day_delivery = "<?php echo $previous_five_day_delivery; ?>";
    var previous_six_day_delivery = "<?php echo $previous_six_day_delivery; ?>";

    var daily_sales_revenue_days = '<?php echo $daily_sales_revenue_days; ?>';

    json_daily_sales_revenue_days = JSON.parse(daily_sales_revenue_days);

    var current_day = '<?php echo date('l, d F Y', strtotime($current_day)); ?>';

    var myChart = echarts.init(document.getElementById('daily_sales_revenue_graph'), theme);
    myChart.setOption({
        title: {
            text: 'Sales Revenue (PKR)',
            subtext: current_day
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['Dine In', 'Take Away', 'Delivery']
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: {
                    show: true
                }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'value',
            boundaryGap: [0, 0.01]
        }],
        yAxis: [{
            type: 'category',
            data: json_daily_sales_revenue_days
        }],
        series: [{
            name: 'Dine In',
            type: 'bar',
            data: [previous_six_day_dine, previous_five_day_dine, previous_four_day_dine, previous_three_day_dine, previous_two_day_dine, previous_one_day_dine, current_day_dine]
        },{
            name: 'Take Away',
            type: 'bar',
            data: [previous_six_day_takeaway, previous_five_day_takeaway, previous_four_day_takeaway, previous_three_day_takeaway, previous_two_day_takeaway, previous_one_day_takeaway, current_day_takeaway]
        },{
            name: 'Delivery',
            type: 'bar',
            data: [previous_six_day_delivery, previous_five_day_delivery, previous_four_day_delivery, previous_three_day_delivery, previous_two_day_delivery, previous_one_day_delivery, current_day_delivery]
        }]
    });
</script>