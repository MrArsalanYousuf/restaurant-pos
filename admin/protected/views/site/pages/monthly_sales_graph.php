<script type="text/javascript">

    var jan_current_dine = "<?php echo $jan_current_dine; ?>";
    var feb_current_dine = "<?php echo $feb_current_dine; ?>";
    var mar_current_dine = "<?php echo $mar_current_dine; ?>";
    var apr_current_dine = "<?php echo $apr_current_dine; ?>";
    var may_current_dine = "<?php echo $may_current_dine; ?>";
    var june_current_dine = "<?php echo $june_current_dine; ?>";
    var july_current_dine = "<?php echo $july_current_dine; ?>";
    var aug_current_dine = "<?php echo $aug_current_dine; ?>";
    var sep_current_dine = "<?php echo $sep_current_dine; ?>";
    var oct_current_dine = "<?php echo $oct_current_dine; ?>";
    var nov_current_dine = "<?php echo $nov_current_dine; ?>";
    var dec_current_dine = "<?php echo $dec_current_dine; ?>";

    var jan_current_takeaway = "<?php echo $jan_current_takeaway; ?>";
    var feb_current_takeaway = "<?php echo $feb_current_takeaway; ?>";
    var mar_current_takeaway = "<?php echo $mar_current_takeaway; ?>";
    var apr_current_takeaway = "<?php echo $apr_current_takeaway; ?>";
    var may_current_takeaway = "<?php echo $may_current_takeaway; ?>";
    var june_current_takeaway = "<?php echo $june_current_takeaway; ?>";
    var july_current_takeaway = "<?php echo $july_current_takeaway; ?>";
    var aug_current_takeaway = "<?php echo $aug_current_takeaway; ?>";
    var sep_current_takeaway = "<?php echo $sep_current_takeaway; ?>";
    var oct_current_takeaway = "<?php echo $oct_current_takeaway; ?>";
    var nov_current_takeaway = "<?php echo $nov_current_takeaway; ?>";
    var dec_current_takeaway = "<?php echo $dec_current_takeaway; ?>";

    var jan_current_delivery = "<?php echo $jan_current_delivery; ?>";
    var feb_current_delivery = "<?php echo $feb_current_delivery; ?>";
    var mar_current_delivery = "<?php echo $mar_current_delivery; ?>";
    var apr_current_delivery = "<?php echo $apr_current_delivery; ?>";
    var may_current_delivery = "<?php echo $may_current_delivery; ?>";
    var june_current_delivery = "<?php echo $june_current_delivery; ?>";
    var july_current_delivery = "<?php echo $july_current_delivery; ?>";
    var aug_current_delivery = "<?php echo $aug_current_delivery; ?>";
    var sep_current_delivery = "<?php echo $sep_current_delivery; ?>";
    var oct_current_delivery = "<?php echo $oct_current_delivery; ?>";
    var nov_current_delivery = "<?php echo $nov_current_delivery; ?>";
    var dec_current_delivery = "<?php echo $dec_current_delivery; ?>";

    var current_day = '<?php echo date('F Y', strtotime($current_day)); ?>';

    var echartLine = echarts.init(document.getElementById('monthly_sales_revenue_graph'), theme);
    echartLine.setOption({
        title: {
            text: 'Sales Revenue (PKR)',
            subtext: current_day
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            x: 220,
            y: 40,
            data: ['Dine In', 'Take Away', 'Delivery']
        },
        toolbox: {
            show: true,
            feature: {
                magicType: {
                    show: true,
                    title: {
                        line: 'Line',
                        bar: 'Bar',
                        stack: 'Stack',
                        tiled: 'Tiled'
                    },
                    type: ['line', 'bar', 'stack', 'tiled']
                },
                restore: {
                    show: true,
                    title: "Restore"
                },
                saveAsImage: {
                    show: true,
                    title: "Save Image"
                }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            boundaryGap: false,
            data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [
        {
            name: 'Dine In',
            type: 'line',
            smooth: true,
            itemStyle: {
                normal: {
                    areaStyle: {
                        type: 'default'
                    }
                }
            },
            data: [jan_current_dine, feb_current_dine, mar_current_dine, apr_current_dine, may_current_dine, june_current_dine, july_current_dine, aug_current_dine, sep_current_dine, oct_current_dine, nov_current_dine, dec_current_dine]
        }, {
            name: 'Take Away',
            type: 'line',
            smooth: true,
            itemStyle: {
                normal: {
                    areaStyle: {
                        type: 'default'
                    }
                }
            },
            data: [jan_current_takeaway, feb_current_takeaway, mar_current_takeaway, apr_current_takeaway, may_current_takeaway, june_current_takeaway, july_current_takeaway, aug_current_takeaway, sep_current_takeaway, oct_current_takeaway, nov_current_takeaway, dec_current_takeaway]
        }, {
            name: 'Delivery',
            type: 'line',
            smooth: true,
            itemStyle: {
                normal: {
                    areaStyle: {
                        type: 'default'
                    }
                }
            },
            data: [jan_current_delivery, feb_current_delivery, mar_current_delivery, apr_current_delivery, may_current_delivery, june_current_delivery, july_current_delivery, aug_current_delivery, sep_current_delivery, oct_current_delivery, nov_current_delivery, dec_current_delivery]
        }]
    });
</script>