<script type="text/javascript">
    var theme = {
        color: ["#0074D9", "#B10DC9", "#2ECC40", "#7FDBFF", "#AAAAAA", "#01FF70", "#3D9970", "#DDDDDD", "#F012BE", "#FF851B", "#01FF70", "#bfd3b7", "FFDC00"],
        title: {
            itemGap: 8,
            textStyle: {
                fontWeight: "normal",
                color: "#408829"
            }
        },
        dataRange: {
            color: ["#1f610a", "#97b58d"]
        },
        toolbox: {
            color: ["#408829", "#408829", "#408829", "#408829"]
        },
        tooltip: {
            backgroundColor: "rgba(0,0,0,0.5)",
            axisPointer: {
                type: "line",
                lineStyle: {
                    color: "#408829",
                    type: "dashed"
                },
                crossStyle: {
                    color: "#408829"
                },
                shadowStyle: {
                    color: "rgba(200,200,200,0.3)"
                }
            }
        },
        dataZoom: {
            dataBackgroundColor: "#eee",
            fillerColor: "rgba(64,136,41,0.2)",
            handleColor: "#408829"
        },
        grid: {
            borderWidth: 0
        },
        categoryAxis: {
            axisLine: {
                lineStyle: {
                    color: "#408829"
                }
            },
            splitLine: {
                lineStyle: {
                    color: ["#eee"]
                }
            }
        },
        valueAxis: {
            axisLine: {
                lineStyle: {
                    color: "#408829"
                }
            },
            splitArea: {
                show: !0,
                areaStyle: {
                    color: ["rgba(250,250,250,0.1)", "rgba(200,200,200,0.1)"]
                }
            },
            splitLine: {
                lineStyle: {
                    color: ["#eee"]
                }
            }
        },
        timeline: {
            lineStyle: {
                color: "#408829"
            },
            controlStyle: {
                normal: {
                    color: "#408829"
                },
                emphasis: {
                    color: "#408829"
                }
            }
        },
        k: {
            itemStyle: {
                normal: {
                    color: "#68a54a",
                    color0: "#a9cba2",
                    lineStyle: {
                        width: 1,
                        color: "#408829",
                        color0: "#86b379"
                    }
                }
            }
        },
        map: {
            itemStyle: {
                normal: {
                    areaStyle: {
                        color: "#ddd"
                    },
                    label: {
                        textStyle: {
                            color: "#c12e34"
                        }
                    }
                },
                emphasis: {
                    areaStyle: {
                        color: "#99d2dd"
                    },
                    label: {
                        textStyle: {
                            color: "#c12e34"
                        }
                    }
                }
            }
        },
        force: {
            itemStyle: {
                normal: {
                    linkStyle: {
                        strokeColor: "#408829"
                    }
                }
            }
        },
        chord: {
            padding: 4,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 1,
                        color: "rgba(128, 128, 128, 0.5)"
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: "rgba(128, 128, 128, 0.5)"
                        }
                    }
                },
                emphasis: {
                    lineStyle: {
                        width: 1,
                        color: "rgba(128, 128, 128, 0.5)"
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: "rgba(128, 128, 128, 0.5)"
                        }
                    }
                }
            }
        },
        gauge: {
            startAngle: 225,
            endAngle: -45,
            axisLine: {
                show: !0,
                lineStyle: {
                    color: [
                    [.2, "#86b379"],
                    [.8, "#68a54a"],
                    [1, "#408829"]
                    ],
                    width: 8
                }
            },
            axisTick: {
                splitNumber: 10,
                length: 12,
                lineStyle: {
                    color: "auto"
                }
            },
            axisLabel: {
                textStyle: {
                    color: "auto"
                }
            },
            splitLine: {
                length: 18,
                lineStyle: {
                    color: "auto"
                }
            },
            pointer: {
                length: "90%",
                color: "auto"
            },
            title: {
                textStyle: {
                    color: "#333"
                }
            },
            detail: {
                textStyle: {
                    color: "auto"
                }
            }
        },
        textStyle: {
            fontFamily: "Arial, Verdana, sans-serif"
        }
    };

    var donut_graph = '<?php echo $donut_graph; ?>';

    json_donut_graph = JSON.parse(donut_graph);

    var current_day = '<?php echo date('l, d F Y', strtotime($current_day)); ?>';

    if ($("#daily_sales_revenue_donut_graph").length) {
        var i = echarts.init(document.getElementById("daily_sales_revenue_donut_graph"), theme);
        i.setOption({
            title: {
              text: "Sales Revenue (PKR)",
              subtext: current_day
            },
            tooltip: {
                trigger: "item",
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            calculable: !0,
            legend: {
                x: "center",
                y: "bottom",
                data: ["Dine in", "Takeaway", "Delivery"],
            },
            toolbox: {
                show: !0,
                feature: {
                    magicType: {
                        show: !0,
                        type: ["pie", "funnel"],
                        option: {
                            funnel: {
                                x: "25%",
                                width: "50%",
                                funnelAlign: "center",
                                max: 1548
                            }
                        }
                    },
                    restore: {
                        show: !0,
                        title: "Restore"
                    },
                    saveAsImage: {
                        show: !0,
                        title: "Save Image"
                    }
                }
            },
            series: [{
                name: "Sales Revenue (PKR)",
                type: "pie",
                radius: ["30%", "55%"],
                itemStyle: {
                    normal: {
                        label: {
                            show: !0
                        },
                        labelLine: {
                            show: !0
                        }
                    },
                    emphasis: {
                        label: {
                            show: !0,
                            position: "center",
                            textStyle: {
                                fontSize: "14",
                                fontWeight: "normal"
                            }
                        }
                    }
                },
                data: json_donut_graph
            }]
        })
    }
</script>