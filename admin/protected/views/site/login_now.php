<?php $acc = Accessories::model()->find(array("condition"=>"1=1 ORDER BY id DESC"));
$favi = $acc->favi_image;
$logo = $acc->logo;
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $acc->title; ?></title>
    <meta name="description" content="php scripts, php, javascript, app templates, app source code, themes, templates, plugins, graphics, wordpress, prestashop, magento, logo templates, android app templates, iOS app templates, unity source code, woocommerce, marketplace, buy, sell" />
    <meta name="keywords" content="Buy Code, Scripts, Themes, Templates and Plugins for PHP, JavaScript, HTML, WordPress, Android, iOS and more." />
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta property="og:title" content="<?php echo $acc->title; ?>" />
    <meta property="og:description" content="Buy Code, Scripts, Themes, Templates and Plugins for PHP, JavaScript, HTML, WordPress, Android, iOS and more." />
    <meta property="og:image" content="<?php echo Yii::app()->baseUrl .'/images/logo/'.$favi; ?>" />

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/build/css/custom.min.css" rel="stylesheet">
    
    <style type="text/css">
        label{
            color: #000;
        }
    }
</style>
</head>
<body class="login" style="background-color: #ebeeef">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form text-center">
                <!-- <i class="fa fa-user fa-5x" aria-hidden="true"></i> -->
                <h1><?php echo $acc->title; ?></h1>
                <p>Login Panel</p>
                <div class="separator">
                    <section class="login_content">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'users-form',
                            'enableAjaxValidation'=>false,

                        )); ?>
                        <form>

                            <div>
                                <?php echo $form->labelEx($model,'username',array('id'=>'formTxt')); ?>
                                <?php echo $form->textField($model,'username',array('class'=>'form-control','size'=>60,'maxlength'=>256)); ?>
                                <?php echo $form->error($model,'username'); ?>
                            </div>
                            <div>
                                <?php echo $form->labelEx($model,'password',array('id'=>'formTxt')); ?>
                                <?php echo $form->passwordField($model,'password',array('class'=>'form-control','size'=>60,'maxlength'=>256)); ?>
                                <?php echo $form->error($model,'password'); ?>
                            </div>
                            <div>
                                <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-danger login-form-btn')); ?>
                                <a class="reset_pass" href="#"></a>
                            </div>

                        </form>
                        <?php $this->endWidget(); ?>
                    </section>

                    <div class="separator">
                        <section class="login_content_with_credentials">

                            <!-- <table class="table table-bordered" style="font-size: 12px">
                            <thead>
                            <tr style="color:#333333;padding: 8px;font: normal 12px Helvetica, Arial, sans-serif">
                            <th>Username</th>
                            <th>Password</th>
                            <th>Role</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="color:#333333;padding: 8px;font: normal 12px Helvetica, Arial, sans-serif">
                            <td>admin</td>
                            <td>123</td>
                            <td>Admin</td>
                            </tr>
                            </tbody>
                            </table> -->

                            <p>Copyright © 2018 All Rights Reserved | <a href="javascript:void(0)" style="color: #009efb;font-weight: 700;font-style: normal;"><?php echo $acc->title; ?></a></p>

                            <p>Developed by</p>

                            <p><a href="javascript:void(0)" target="_blank" style="color: red;font-weight: 700;font-style: normal;">ARSALAN YOUSUF</a></p>

                        </section>
                    </div>
                </div>
            </div>
        </body>
        </html>