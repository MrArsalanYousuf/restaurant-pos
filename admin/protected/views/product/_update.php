<style type="text/css">
    .errorMessage {
        color: red;
        font-weight: 700;
    }
    
    .modal-header {
        padding: 10px 40px !important;
        border-bottom: 1px solid #e5e5e5 !important;
    }

    .modal.modal-wide .modal-dialog {
        width: 80%;
        overflow-y: initial !important;
    }
    
    .modal-wide .modal-body {
        overflow-y: auto;
        height: 350px;
    }
    
    #myModal .modal-body p {
        margin-bottom: 900px
    }
    
    .fade-scale {
        transform: scale(0);
        opacity: 0;
        -webkit-transition: all .25s linear;
        -o-transition: all .25s linear;
        transition: all .25s linear;
    }
    
    .fade-scale.in {
        opacity: 1;
        transform: scale(1);
    }

    button.close {
        -webkit-appearance: none;
        background: #f30;
        border: 1px solid #f30;
        cursor: pointer;
        padding: 2px 6px 4px 6px;
    }
    .close{
        color: #FFF;
        font-size: 1.3em;
        line-height: 1.2em;
        opacity: 1;
        position: absolute;
        right: 15px;
        top: 15px;
        font-weight: 900;
    }
</style>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h1 style="color: #000;font-size: 29px;"><?php echo $model->isNewRecord ? 'Create Product' : 'Update Product' ?></h1>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'product-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
      )); ?>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'cat_id'); ?>
                                    <?php echo $form->dropDownList($model,'cat_id', CHtml::listData(Category::model()->findAll(array("condition"=>"status = 1")),'id','name'), array('empty'=>'Select Category...','class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'cat_id'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'name'); ?>
                                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'name'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'code'); ?>
                                    <?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'code'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'price'); ?>
                                    <?php echo $form->textField($model,'price',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'price'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'description'); ?>
                                    <?php echo $form->textArea($model,'description',array('rows'=>8,'class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'description'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'image'); ?>
                                    <?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                                        <?php echo $form->error($model,'image'); ?>
                            </div>
                        </div>
                    </div>

                    <?php if(!empty($model->image)){ ?>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                
                            <a href='<?php echo Yii::app()->baseUrl .' /images/products/ '.$model->image; ?>' target='_blank'>
                            <img src="<?php echo Yii::app()->baseUrl .'/images/products/'.$model->image; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/images/product4.jpg'" alt="" width="90" height="90" style="border: 1px solid #000" />
                            </a>
                                    
                            </div>
                        </div>
                    </div>

                    <?php } ?>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo $form->labelEx($model,'size'); ?>
                                    <?php $temp_list = array('No'=>'No','Yes'=>'Yes'); ?>
                                        <?php echo $form->dropDownList($model,'size', $temp_list,array('value'=>$model->size,'empty'=>'Select Size...','class'=>'form-control size')); ?>
                                            <?php echo $form->error($model,'size'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <?php if($model->size == "Yes"){ ?>
                                    <button type="button" class="btn btn-danger open_modal">Show Sizes</button>
                                <?php } ?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 1%">
                            <div class="form-group">
                                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
                            </div>
                        </div>
                    </div>

                    <div class="modal modal-wide fade-scale" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header" style="color: #000;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h3 class="modal-title">Add Sizes</h3>
                                </div>
                                <div class="modal-body">

                                    <div class="container-fluid">
                                        <div class="row timing_div">
                                            <div class="col-md-12 text-center">

                                                <?php $result = ""; 
        $sizes = Product::model()->findAll(array("condition"=>"product_id = $model->id"));
        $size = Size::model()->findAll();

        if(!empty($sizes)){
        $result = count($sizes);
        foreach ($sizes as $key => $value) { ?>

                                                    <div class="col-md-3">
                                                        <label>Name*</label>
                                                        <input type="text" class="form-control" name="Product[<?php echo $key; ?>][name]" value="<?php echo $value->name; ?>">
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label>Size*</label>
                                                        <select class="form-control" name="Product[<?php echo $key; ?>][size]">
                                                            <option value="">--- Please Select ---</option>
                                                            <?php foreach($size as $k => $v) { ?>
                                                            <option value="<?php echo $v->name; ?>" <?php if($value->size == $v->name){echo "selected"; } ?>><?php echo $v->name; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <label>Price*</label>
                                                        <input type="text" class="form-control" name="Product[<?php echo $key; ?>][price]" value="<?php echo $value->price; ?>">
                                                    </div>

                                                    <div class='col-md-3' style='margin-top:23px;'>
                                                        <button class='add_more_fields btn btn-primary'>Add More </button>
                                                    </div>

                                                    <?php } } else {  ?>

                                                        <div class="col-md-3">
                                                            <label>Name*</label>
                                                            <input type="text" class="form-control" name="Product[0][name]" />
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Size*</label>
                                                            <select class="form-control" name="Product[0][size]">
                                                                <?php foreach($size as $k => $v) { ?>
                                                                <option value="<?php echo $v->name; ?>"><?php echo $v->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>Price*</label>
                                                            <input type="text" class="form-control" name="Product[0][price]" />
                                                        </div>

                                                        <div class='col-md-3' style='margin-top:23px;'>
                                                            <button class='add_more_fields btn btn-primary'>Add More</button>
                                                        </div>

                                                        <?php } ?>
                                            </div>
                                            <!-- col-md-12 -->
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                    </div>

                                    <br/>

                                </div>
                                <!-- Modal Body -->

                            </div>

                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(".open_modal").click(function() {

        $('#myModal').modal('show');

    });

    $(".size").change(function() {

        selected_value = $(this).val();

        if (selected_value == "Yes") {
            $('#myModal').modal('show');
        } else {
            $('#myModal').modal('hide');
        }

    });
</script>

<?php if(!empty($sizes)){ ?>

    <script>
        counter = <?php echo $result; ?>;

        $("body").on('click', '.add_more_fields', function(e) {

            e.preventDefault();
            name_attr = $(this).attr('data-names');
            parent_parent_div = $(this).closest('.timing_div').parent();
            new_div = "";

            new_div += "<div class='timing_div row'>";
            new_div += "<div class='col-md-12 text-center'>";

            new_div += "<div class='col-md-3'><label>Name*</label>";
            new_div += "<input class='form-control' required='required'  type='text' name='Product[" + counter + "][name]' />&nbsp;</div>";

            new_div += "<div class='col-md-3'><label>Size*</label>";
            new_div += "<select class='form-control size' name='Product[" + counter + "][size]'>";
            new_div += "<option value=''>Select Size</option>";
        
                <?php foreach($size as $k => $v) { ?>
                    new_div += "<option value='<?php echo $v->name; ?>'><?php echo $v->name; ?></option>";
                <?php } ?>

            new_div += "</select></div>";

            new_div += "<div class='col-md-3'><label>&nbsp;Price *</label>";
            new_div += "<input class='form-control' required='required'  type='text' name='Product[" + counter + "][price]' />&nbsp;</div>";

            new_div += "<div class='col-md-3' style='margin-top: 23px;'><button class='add_more_fields btn btn-primary'><i class='fa fa-plus'></i></button>";
            new_div += "<button class='delete_timing_input_field btn btn-danger'><i class='fa fa-trash-o'></i></button></div>";
            new_div += " </div>";
            new_div += " </div>";

            $(parent_parent_div).append(new_div);

            counter++;

        });
    </script>

    <?php } else { ?>

        <script>
            counter = 0;

            $("body").on('click', '.add_more_fields', function(e) {

                counter++;

                e.preventDefault();
                name_attr = $(this).attr('data-names');
                parent_parent_div = $(this).closest('.timing_div').parent();
                new_div = "";

                new_div += "<div class='timing_div row'>";
                new_div += "<div class='col-md-12 text-center'>";

                new_div += "<div class='col-md-3'><label>Name*</label>";
                new_div += "<input class='form-control' required='required'  type='text' name='Product[" + counter + "][name]' />&nbsp;</div>";

                new_div += "<div class='col-md-3'><label>Size*</label>";
                new_div += "<select class='form-control size' name='Product[" + counter + "][size]'>";
                new_div += "<option value=''>Select Size</option>";
        
                <?php foreach($size as $k => $v) { ?>
                    new_div += "<option value='<?php echo $v->name; ?>'><?php echo $v->name; ?></option>";
                <?php } ?>

                new_div += "</select></div>";

                new_div += "<div class='col-md-3'><label>&nbsp;Price *</label>";
                new_div += "<input class='form-control' required='required'  type='text' name='Product[" + counter + "][price]' />&nbsp;</div>";

                new_div += "<div class='col-md-3' style='margin-top: 23px;'><button class='add_more_fields btn btn-primary'><i class='fa fa-plus'></i></button>";
                new_div += "<button class='delete_timing_input_field btn btn-danger'><i class='fa fa-trash-o'></i></button></div>";
                new_div += " </div>";
                new_div += " </div>";

                $(parent_parent_div).append(new_div);

            });
        </script>

        <?php } ?>

            <script>
                $("body").on('click', '.delete_timing_input_field', function(e) {

                    e.preventDefault();

                    parent_parent_div = $(this).closest('.timing_div').remove();

                });
            </script>