<style type="text/css">
	th,td {text-align:center}
</style>
<div class="row">
   <div class="col-md-12 col-xs-12">
      <div class="x_panel">
       	<div class="row">
            <div class="col-md-12 col-xs-12 text-center">
            		<h2 style="font-size: 50px;font-family: Lucida Bright;"><?php echo $model->name; ?></h2>
            </div>
        </div>

        <br>


        <div class="row">
            <div class="col-md-12 col-xs-12 text-center">
            	<?php if(!empty($model->image)){ ?>
                    <a href='<?php echo Yii::app()->baseUrl .'/images/products/'.$model->image; ?>' target='_blank'>
                        <img style='width:40%;' src="<?php echo Yii::app()->baseUrl .'/images/products/'.$model->image; ?>" />
                    </a>
                <?php } ?>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-12 col-xs-12">
            	<div class="container-fluid">
            	<table class="table table-striped table-bordered table-hover">

							<tr>
								<th>Category Name:</th>
								<td><?php echo $model->category->name; ?></td>
							</tr>

							<tr>
								<th>Name:</th>
								<td><?php echo $model->name; ?></td>
							</tr>

							<tr>
								<th>Code:</th>
								<td><?php echo $model->code; ?></td>
							</tr>

							<tr>
								<th>Price:</th>
								<td><?php echo $model->price; ?></td>
							</tr>

                            <tr>
                                <th>Description:</th>
                                <td><?php echo $model->description; ?></td>
                            </tr>

                            <tr>
                                <th>Status:</th>
                                <td><?php if($model->status == 1){ ?>
                                    <p style="color: #27883d; font-weight: 600" class="active">Active</p>
                                <?php } else { ?>
                                    <p style="color: #b61007; font-weight: 600" class="suspend">Suspend</p>
                                <?php } ?>
                                </td>
                            </tr>

							<tr>
								<th>Creation Date:</th>
								<td><?php echo date('d-M-Y', strtotime($model->date_added)) ?></td>
							</tr>
            </table>
            </div>
            </div>
        </div>

       </div>
   </div>
</div>
