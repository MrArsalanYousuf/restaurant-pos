<div class="row">
<div class="col-md-12 col-xs-12">
<div class="x_title">

<h1>Manage Products</h1>
<ul class="nav navbar-right panel_toolbox">
  <a href="<?php echo Yii::app()->createUrl('Product/create'); ?>" class="btn btn-danger" style="float:right;border-radius:5px;">
  <i class="fa fa-pencil"></i> Create Product</a>
</ul>

<div class="clearfix"></div>
</div>
                  
<div class="x_content">
<?php $form=$this->beginWidget('CActiveForm', array(
  'action'=>Yii::app()->createUrl($this->route),
  'method'=>'get',
)); ?>

                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $form->label($model,'cat_id'); ?>
                        <?php echo $form->dropDownList($model,'cat_id', CHtml::listData(Category::model()->findAll(array("condition"=>"status = 1")),'id','name'), array('empty'=>'Choose Here','class'=>'form-control')); ?>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                        <?php echo $form->label($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                    </div>


                      <div class="form-group">
                        <div class="col-md-10 col-sm-9 col-xs-12">
                          <?php echo CHtml::submitButton('Search',array('class'=>'btn btn-success')); ?>
                        </div>
                      </div>

<?php $this->endWidget(); ?>
</div>

</div>
</div>