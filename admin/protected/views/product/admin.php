<?php

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<br>

<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Products Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>

                        <th scope="col" style="width: 100px !important">
							Image
						</th>

                        <th scope="col" style="width: 300px !important">
							Category
						</th>

                        <th scope="col" style="width: 500px !important">
							Name
						</th>

						<th scope="col" style="width: 300px !important">
							Code
						</th>

						<th scope="col" style="width: 300px !important">
							Price
						</th>

						<th scope="col" style="width: 300px !important">
							Sizes
						</th>

						<th scope="col" style="width: 300px !important">
							Status
						</th>

						<th scope="col" style="width: 450px !important">
							Action
						</th>

                    </tr>
                  </thead>
                  <tbody>
                      			<?php $this->widget('zii.widgets.CListView', array(
									'dataProvider'=>$model->search(),
									'itemView'=>'_view',
								)); ?>
                  </tbody>

                    </table>
                    </div>

                    			<div class='div-pagination Personal_info'></div>
								<style>
								.page_info{
									display:none !important;
								}
								.div-pagination.Personal_info{
									text-align:center;
								}
								.yiiPager>li>a {
									    position: relative;
   
										padding: 6px 12px !important;
										margin-left: -1px !important;
										line-height: 1.42857143 !important;
										color: #3b9c96 !important;
										text-decoration: none !important;
										background-color: #fff !important;
										border: 1px solid #ddd !important;
										border-radius: 2px !important;
								}
								ul.yiiPager .selected a {
									color:#fff !important;
									background-color:#3b9c96 !important;
								}
								.pager .next>a, .pager .next>span {
									float:none;
								}
								/*ul.yiiPager .first, ul.yiiPager .last {
									display: block !important;
								}*/
								.pager .previous>a, .pager .previous>span {
									float:none;
								}
								.pager {
									font-size:0px;
								}
								
								</style>

                  </div>
                </div>
              </div>
             </div>
           </div>

<script>
$('.search-button').click(function(){
	
	$('.search-form').toggle();
	return false;
});
</script>


<script>
$(document).ready(function(){

	$(".suspend_pro").click(function() {
	
	current_row = $(this);

	suspend_id = $(this).attr('data-id');

	var r = confirm("Do you really want to suspend this Product ?");
    if (r == false) {
        return;
    }

	  $.ajax({ 
	  type: "POST",
	  url: "<?php echo Yii::app()->createUrl('Product/SuspendCategory'); ?>",
	  data: {suspend_id: suspend_id},
	  success: function(data){ 

	  if(data == 'success'){
	  		current_row.closest('tr').find('.active').text(" ");
			 current_row.closest('tr').find('.active').append( "<p style='color: #b61007; font-weight: 600'>Suspend</p>" );
		}else if(data == 'fail'){
			alert("Something Wrong");
		}
	},
	 error: function(xhr, status, error) {
			alert(xhr.responseText);
		} 
		});
});



$(".active_pro").click(function() {
	
	current_row = $(this);

	active_id = $(this).attr('data-id');

	var r = confirm("Do you really want to active this Product ?");
    if (r == false) {
        return;
    }

	  $.ajax({ 
	  type: "POST",
	  url: "<?php echo Yii::app()->createUrl('Product/ActiveCategory'); ?>",
	  data: {active_id: active_id},
	  success: function(data){ 

	  if(data == 'success'){
	  		current_row.closest('tr').find('.suspend').text(" ");
			 current_row.closest('tr').find('.suspend').append( "<p style='color: #27883d; font-weight: 600'>Active</p>" );
		}else if(data == 'fail'){
			alert("Something Wrong");
		}
	},
	 error: function(xhr, status, error) {
			alert(xhr.responseText);
		} 
		});
});

	
	
});
</script>