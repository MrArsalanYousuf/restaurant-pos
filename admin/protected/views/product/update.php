<?php

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php $this->renderPartial('_update', array('model'=>$model)); ?>