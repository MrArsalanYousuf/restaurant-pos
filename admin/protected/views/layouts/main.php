<style type="text/css">
    .right_col{
        min-height: 100% !important;
    }
    .btn{
        padding: 8px 12px !important;
        height: 35px !important;
    }
    th,td{
        color: #333 !important;
    }
    body{
        font-family: helvetica !important
    }
</style>

<?php $user = Yii::app()->session['user'];
$user_type = isset($user->user_type)?$user->user_type:'';

$id = (int)Yii::app()->user->id;
if($id > 0){
    $userdata = Users::model()->find(array("condition"=>"id = $id"));
    $image = $userdata->image;
} ?>


<?php $acc = Accessories::model()->find(array("condition"=>"1=1 ORDER BY id DESC"));
$favi = $acc->favi_image;
$logo = $acc->logo;
?>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $acc->title; ?></title>

    <!-- jQuery -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/jquery/dist/jquery.min.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/echart/green.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/echart/echarts-all.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/echarts/echarts.min.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/morris/morris.min.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/raphael/raphael.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>

    <!-- Bootstrap -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/build/css/custom.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/autocomplete/jquery-auto-complete.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl .'/images/logo/'.$favi; ?>"/>

</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?php echo Yii::app()->createUrl("site/index"); ?>" class="site_title"><i class="fa fa-phone fa-lg"></i> <span>CMS</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="<?php echo Yii::app()->baseUrl .'/images/users/call-centre.png'; ?>" alt="..." class="img-circle profile_img" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/images/not_found.png'" />
                        </div>
                        <div class="profile_info">
                            <span>Welcome</span>
                            <h2><?php echo Yii::app()->user->name; ?></h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">

                                <li><a href="<?php echo Yii::app()->createUrl("/"); ?>"><i class="fa fa-tachometer"></i> Dashboard </a></li>


                                <li><a><i class="fa fa-tags"></i> Categories <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("Category/admin"); ?>">Manage Categories</a></li>

                                    </ul>
                                </li>

                                <li><a><i class="fa fa-cutlery"></i> Products <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("Product/admin"); ?>">Manage Products</a></li>

                                    </ul>
                                </li>

                                <li><a><i class="fa fa-shopping-cart"></i> Orders <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("Checkout/admin"); ?>">Manage Orders</a></li>

                                    </ul>
                                </li>

                                <li><a><i class="fa fa-line-chart"></i> Reports <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("Checkout/Items"); ?>">Top Items</a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl("Checkout/Sales"); ?>">Sales Summary</a></li>

                                    </ul>
                                </li>

                                <li><a><i class="fa fa-users fa-lg"></i> Users <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("users/admin"); ?>">Manage Users</a></li>

                                    </ul>
                                </li>

                                <li><a><i class="fa fa-cog fa-lg"></i> Setup <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">

                                        <li><a href="<?php echo Yii::app()->createUrl("Accessories/admin"); ?>">Accessories</a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl("Size/admin"); ?>">Sizes</a></li>

                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->


                </div> <!-- left_col scroll-view -->
            </div> <!-- col-md-3 left_col -->

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo Yii::app()->baseUrl .'/images/users/'.$image; ?>" alt="" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/images/not_found.png'"/><?php echo Yii::app()->user->name; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">

                                    <li><?php echo CHtml::link("<i class='fa fa-arrow-right'></i> Log Out", array('site/logout')); ?></li>

                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <!-- top tiles -->

                <div class="flash" style="padding-top: 60px">
                    <?php $flashMessages = Yii::app()->user->getFlashes();
                    if ($flashMessages) {
                        foreach($flashMessages as $key => $message) {  
                            echo '<div style="text-align: center;color: #FFF" class="alert main-alert alert-' . $key . ' fade in">'.$message.'</div>';
                        }
                    };
                    ?>
                </div>

                <?php echo $content; ?>
                <!-- BEGIN PAGE HEAD -->

            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer class="footer">
                <div class="pull-right">
                    © 2017 All Rights Reserved. Point of Sale Design & Developed by by <a href="javascript:void(0)" style="color: red">Arsalan Yousuf</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->

        </div> <!-- main_container -->
    </div> <!-- container body -->

    <!-- bootstrap-datepicker -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/shahzaib/autocomplete/jquery-auto-complete.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/build/js/custom.min.js"></script>


    <script>
        window.setTimeout(function() {
            $(".main-alert").fadeTo(500, 0).slideUp(600, function(){
                $(this).remove(); 
            });
        }, 2000);
    </script>

</body>
<!-- END BODY -->
</html>