<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>