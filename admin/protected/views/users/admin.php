<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<br>


<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Employee Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>

                        <th scope="col" style="width: 50px !important;">
							Image
						</th>
									
						<th scope="col" style="width: 80px !important;">
							Username
						</th>
									
						<th scope="col" style="width: 120px !important;">
							Name
						</th>
									
						<th scope="col" style="width: 120px !important;">
							Last Name
						</th>
									
						<th scope="col" style="width: 120px !important;">
							Email
						</th>

						<th scope="col" style="width: 120px !important;">
							User type
						</th>
							
						<th scope="col" style="width: 120px !important;">
							Status
						</th>		
		

						<th scope="col" style="width: 250px !important;">
							Action
						</th>

                    </tr>
                  </thead>
                  <tbody>
                      			<?php $this->widget('zii.widgets.CListView', array(
									'dataProvider'=>$model->search(),
									'itemView'=>'_view',
								)); ?>
                  </tbody>

                    </table>
                    </div>
                  </div>
                </div>
              </div>
             </div>
           </div>


<script>
$('.search-button').click(function(){
	
	$('.search-form').toggle();
	return false;
});
</script>

<script>
$(document).ready(function(){

	$(".ban").click(function() {
	
	current_row = $(this);

	id = $(this).attr('data-id');

	var r = confirm(" Do you really want to Deactivate this User ? ");
    if (r == false) {
        return;
    }

$.ajax({ 
	  type: "POST",
	  url: "<?php echo $this->createUrl('Users/DeactivateUser'); ?>",
	  data: {id: id},
	  success: function(data){ 
	  if(data == 'success'){
		current_row.closest('tr').remove();
		alert("Deactivated Successfully");
		}
		else if(data == 'fail'){
			alert("Something went Wrong");	
		}
	},
	 error: function(xhr, status, error) {
			alert(xhr.responseText);
		} 
		});
});


$(".active").click(function() {
	
	current_row = $(this);

	id = $(this).attr('data-id');

	var r = confirm(" Do you really want to Activate this User ? ");
    if (r == false) {
        return;
    }

$.ajax({ 
	  type: "POST",
	  url: "<?php echo $this->createUrl('Users/ActivateUser'); ?>",
	  data: {id: id},
	  success: function(data){ 
	  if(data == 'success'){
	  		current_row.closest('tr').find('.deactive_user').text(" ");
			current_row.closest('tr').find('.deactive_user').append( "<p style='color: #27883d; font-weight: 600'>Active</p>" );
		}
		else if(data == 'fail'){
			alert("Something went Wrong");	
		}
	},
	 error: function(xhr, status, error) {
			alert(xhr.responseText);
		} 
		});
});

});
</script>