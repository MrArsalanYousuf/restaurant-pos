<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<tr>

  <td>
  <a href="<?php echo Yii::app()->CreateUrl('Users/View/'.$data->id)?>">
	<img src="<?php echo Yii::app()->baseUrl .'/images/users/'.$data->image; ?>" onerror="this.src='<?php echo Yii::app()->baseUrl; ?>/images/not_found.png'" alt="" width="50" height="50" />
	</a>
     </td>
	
    <td>
	<?php echo CHtml::encode($data->username); ?>
    </td>
	
	 <td>
		<?php echo CHtml::encode($data->first_name); ?>
    </td>
	 
	 <td>
		<?php echo CHtml::encode($data->last_name); ?>
     </td>
	 
	 <td>
		<?php echo CHtml::encode($data->email); ?>
     </td>

     <td>
		<?php echo CHtml::encode($data->user_type); ?>
     </td>
	 
    <td>
	  <?php if($data->status == 1){ ?>
        <p style="color: #27883d; font-weight: 600" class="active_user">Active</p>
      <?php } else { ?>
      	<p style="color: #b61007; font-weight: 600" class="deactive_user">Deactive</p>
      <?php } ?>
     </td>
 

	 <td>
		<a href="<?php echo Yii::app()->CreateUrl('Users/Update/'.$data->id)?>" class="fa fa-pencil btn btn-primary"></a>
		<a href="<?php echo Yii::app()->CreateUrl('Users/View/'.$data->id)?>" class="fa fa-eye btn btn-info"></a>

		<!-- <a href="<?php echo Yii::app()->CreateUrl('Users/DeleteRow/'.$data->id)?>" class="fa fa-trash-o btn btn-danger"></a> -->
		
		<?php if($data->status == 1){ ?>
		<a href="javascript:void(0)" data-id='<?php echo $data->id; ?>' class="fa fa-ban btn btn-danger ban" data-toggle="tooltip" data-placement="top" title="Deactivate"></a>
		<?php } else { ?>
		<a href="javascript:void(0)" data-id='<?php echo $data->id; ?>' class="fa fa-check btn btn-success active" data-toggle="tooltip" data-placement="top" title="Active"></a>
		<?php } ?>
	</td>
  </tr>