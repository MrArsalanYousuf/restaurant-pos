<div class="row">
   <div class="col-md-12 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
             <h1>Create Users</h1>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<?php echo $form->errorSummary($model); ?>
					
					<div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
					<?php echo $form->error($model,'username'); ?>
          </div>


          <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'first_name'); ?>
          <?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
					<?php echo $form->error($model,'first_name'); ?>
          </div>

          <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'last_name'); ?>
          <?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
					<?php echo $form->error($model,'last_name'); ?>
          </div>

          <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'email'); ?>
          <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
					<?php echo $form->error($model,'email'); ?>
          </div>

          <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'password'); ?>
          <?php 
							if($model->isNewRecord){
								$attributes = array('size'=>60,'maxlength'=>256,'class'=>'form-control'); 
							}else{
								$attributes = array('size'=>60,'maxlength'=>256,'class'=>'form-control','disabled'=>'disabled','value'=>'***************','readonly'=>'readonly'); 
							}
						?>
		        <?php echo $form->passwordField($model,'password', $attributes); ?>
            </div>

          <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'user_type'); ?>
          <?php $temp_list =  array('superadmin'=>'Super Admin', 'admin'=>'Admin', 'manager'=>'Manager', 'operator'=>'Computer Operator', 'waiter'=>'Waiter'); ?>
          <?php echo $form->dropDownList($model,'user_type', $temp_list, array('value'=>$model->user_type,'empty'=>'Please Select','class'=>'form-control')); ?>
          <?php echo $form->error($model,'user_type'); ?>
          </div> 

            <div class="col-md-8 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
            <?php echo $form->labelEx($model,'image'); ?>
						<?php echo $form->fileField($model,'image',array('rows'=>3, 'cols'=>30,'class'=>'form-control')); ?>
						<?php echo $form->error($model,'image'); ?>
            </div>

                      <div class="form-group" style="margin-top: 1%">
                        <div class="col-md-8 col-sm-9 col-xs-12">
                          <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
                        </div>
                      </div>

                    <?php $this->endWidget(); ?>
                  </div>
                </div>
               </div>
              </div>