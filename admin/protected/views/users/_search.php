<div class="row">
<div class="col-md-12 col-xs-12">
<div class="x_title">
       <h1>Manage Employee Information</h1>
<ul class="nav navbar-right panel_toolbox">
	<a href="<?php echo Yii::app()->createUrl('users/create'); ?>" class="btn btn-danger" style="float:right">
	<i class="fa fa-plus"></i> Add Employee</a>
</ul>

<div class="clearfix"></div>
</div>
                  
<div class="x_content">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
					
					<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" style="margin-top: 1%">
          <?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                     </div>


                      <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
                      <?php echo $form->labelEx($model,'first_name'); ?>
                        <?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                      </div>

                       <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
                       <?php echo $form->labelEx($model,'last_name'); ?>
                        <?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 1%">
                      <?php echo $form->labelEx($model,'last_name'); ?>
                        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
                      </div>

                      <div class="form-group">
                        <div class="col-md-8 col-sm-9 col-xs-12">
                          <?php echo CHtml::submitButton('Search',array('class'=>'btn btn-success')); ?>
                        </div>
                      </div>

<?php $this->endWidget(); ?>
</div>

</div>
</div>
