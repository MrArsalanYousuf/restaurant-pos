<?php
/* @var $this AccessoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accessories',
);

$this->menu=array(
	array('label'=>'Create Accessories', 'url'=>array('create')),
	array('label'=>'Manage Accessories', 'url'=>array('admin')),
);
?>

<h1>Accessories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
