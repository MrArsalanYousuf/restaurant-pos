<style type="text/css">
	.errorMessage{
		color: red;
		font-weight: 600;
	}
</style>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h1><?php echo ($model->isNewRecord ? "Create Information" : "Update Information"); ?></h1>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'users-form',
					'enableAjaxValidation'=>true,
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
				)); ?>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'logo'); ?>
					<?php echo $form->fileField($model,'logo',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
					<?php echo $form->error($model,'logo'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'favi_image'); ?>
					<?php echo $form->fileField($model,'favi_image',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
					<?php echo $form->error($model,'favi_image'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'title'); ?>
					<?php echo $form->textField($model,'title',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'title'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'phone_no'); ?>
					<?php echo $form->textField($model,'phone_no',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'phone_no'); ?>
				</div>


				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'mobile_no'); ?>
					<?php echo $form->textField($model,'mobile_no',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'mobile_no'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'email'); ?>
					<?php echo $form->textField($model,'email',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'email'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'footer_name'); ?>
					<?php echo $form->textField($model,'footer_name',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'footer_name'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'discount'); ?>
					<?php echo $form->textField($model,'discount',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'discount'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'type'); ?>
					<?php $temp_list = array('percentage'=>'Percentage','exect'=>'Exect'); ?>
					<?php echo $form->dropDownList($model,'type', $temp_list, array('value'=>$model->type,'empty'=>'Select Type...','class'=>'form-control')); ?>
					<?php echo $form->error($model,'type'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'tax'); ?>
					<?php echo $form->textField($model,'tax',array('class'=>'form-control','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'tax'); ?>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top: 1%">
					<?php echo $form->labelEx($model,'address'); ?>
					<?php echo $form->textArea($model,'address',array('class'=>'form-control','rows'=>6, 'cols'=>50)); ?>
					<?php echo $form->error($model,'address'); ?>
				</div>

				<div class="form-group" style="margin-top: 1%">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
					</div>
				</div>

				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>