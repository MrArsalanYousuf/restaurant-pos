<?php
/* @var $this AccessoriesController */
/* @var $model Accessories */

$this->breadcrumbs=array(
	'Accessories'=>array('index'),
	$model->title,
);

 
?>

<h1>View Accessories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'logo',
		'favi_image',
		'title',
		'email',
		'footer_name',
		'address',
		'date_added',
	),
)); ?>
