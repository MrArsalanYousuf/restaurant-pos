<?php
/* @var $this AccessoriesController */
/* @var $model Accessories */

$this->breadcrumbs=array(
	'Accessories'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#accessories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Restaurant Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>

                        <th scope="col" style="width: 50px !important;">
							Title
						</th>
									
						<th scope="col" style="width: 80px !important;">
							Logo
						</th>
									
						<th scope="col" style="width: 120px !important;">
							Favicon
						</th>

						<th scope="col" style="width: 250px !important;">
							Action
						</th>

                    </tr>
                  </thead>
                  <tbody>
                      	<?php $this->widget('zii.widgets.CListView', array(
							'dataProvider'=>$model->search(),
							'itemView'=>'_view',
						)); ?>
                  </tbody>

                    </table>
                    </div>
                  </div>
                </div>
              </div>
             </div>
           </div>