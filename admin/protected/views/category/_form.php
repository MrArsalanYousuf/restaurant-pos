<style type="text/css">
	.errorMessage{
		color: red;
		font-weight: 700;
	}
</style>

<div class="row">
 <div class="col-md-12 col-xs-12">
  <div class="x_panel">
   <div class="x_title">
     <h1 style="color: #000;font-size: 29px;"><?php echo ($model->isNewRecord ? 'Create Category' : 'Update Category'); ?></h1>
     <div class="clearfix"></div>
   </div>
   <div class="x_content">

     <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'pms-work-drawing-form',
      'enableAjaxValidation'=>false,
      'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <?php echo $form->labelEx($model,'name'); ?>
          <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
          <?php echo $form->error($model,'name'); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
          <?php echo $form->labelEx($model,'priority'); ?>
          <?php echo $form->textField($model,'priority',array('class'=>'form-control')); ?>
          <?php echo $form->error($model,'priority'); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="form-group">
          <?php echo $form->labelEx($model,'description'); ?>
          <?php echo $form->textArea($model,'description',array('rows'=>12,'class'=>'form-control')); ?>
          <?php echo $form->error($model,'description'); ?>
        </div>
        
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">

        <div class="form-group">
          <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn btn-success')); ?>
        </div>

      </div>
    </div>

    <?php $this->endWidget(); ?>
  </div>
</div>
</div>
</div>