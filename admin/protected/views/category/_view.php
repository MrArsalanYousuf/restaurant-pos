<tr>
	
    <td>
    <?php echo CHtml::encode($data->name); ?>
    </td>

    <td>
    <?php echo CHtml::encode($data->priority); ?>
    </td>

    <td>
    <?php echo CHtml::encode($data->description); ?>
    </td>
    
    <td>
     <?php if($data->status == 1){ ?>
        <p style="color: #27883d; font-weight: 600" class="active">Active</p>
      <?php } else { ?>
        <p style="color: #b61007; font-weight: 600" class="suspend">Suspend</p>
      <?php } ?>
    </td>
    
    <td>
        <a href="<?php echo Yii::app()->CreateUrl('Category/update/'.$data->id)?>" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-pencil"></i></a>

        <a href="<?php echo Yii::app()->CreateUrl('Category/view/'.$data->id)?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>

        <?php if($data->status == 1){ ?>

        <a href="javascript:void(0)" class="btn btn-danger suspend_cat" data-id='<?php echo $data->id; ?>' data-toggle="tooltip" data-placement="top" title="Suspend"><i class="fa fa-ban"></i></a>

        <?php } else { ?>

        <a href="javascript:void(0)" class="btn btn-warning active_cat" data-id='<?php echo $data->id; ?>' data-toggle="tooltip" data-placement="top" title="Active"><i class="fa fa-check"></i></a>

        <?php } ?>
    </td> 

  </tr>