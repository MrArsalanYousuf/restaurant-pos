<?php
/* @var $this CheckoutController */
/* @var $model Checkout */

$this->breadcrumbs=array(
	'Checkouts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>