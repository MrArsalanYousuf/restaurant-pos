<div class="x_title">

<h1>Monthly Sales Summary</h1>

<div class="clearfix"></div>
</div>

<form method="get" action="<?php echo $this->createUrl('Checkout/Sales'); ?>">

<div class="row">
<div class="col-md-4">
<label>Date From</label>
<input type="text" class="form-control datepicker1" name="date_search_from" value="<?php echo (isset($_GET['date_search_from']) ? $_GET['date_search_from'] : ""); ?>" autocomplete="off" required="true" />
</div>

<div class="col-md-4">
<label>Date To</label>
<input type="text" class="form-control datepicker1" name="date_search_to" value="<?php echo (isset($_GET['date_search_to']) ? $_GET['date_search_to'] : ""); ?>" autocomplete="off" required="true" />
</div>
</div>

<div class="row">
<div class="col-md-12">
<br>
<input type="submit" class="btn btn-success" name="submit" value="Search"/>
<a href="javascript:void(0)" class="btn btn-primary pull-right" id="print_out_this_page">Print <i class="fa fa-print"></i></a>
<a href="<?php echo Yii::app()->createUrl('Checkout/Sales'); ?>" class="btn btn-danger pull-right">Reload <i class="fa fa-refresh"></i></a>
</div>
</div>
</form>

<script>
 $(function() {
	
	 $(document).on('focusin', '.datepicker1', function(){
      $(this).datepicker({
		  dateFormat: $.datepicker.W3C,
		  changeMonth: true,
		  changeYear: true,
		  yearRange: '1900:<?php echo date('Y')?>'
		});
    });
	
  });
</script>