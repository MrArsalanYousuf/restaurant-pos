<?php
$counter = 0;
?>

<div class="row">
   <div class="col-md-12 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
             <h1 style="color: #000;font-size: 29px;">Update Order</h1>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'checkout-form',
                        'enableAjaxValidation'=>false,
                    )); ?>

                    <?php $orders = OrderProduct::model()->findAll(array("condition"=>"checkout_id = $model->id")); ?>

                    <div class="row">
                      <div class="col-xs-12">
                        
                            <div class="table-responsive">
                                          
                            <table class="table table-striped table-hover table-bordered">
                                                  
                                <thead>
                                <tr>
                                    <th style="width: 45%;">Product Name</th>
                                    <th style="width: 10%;">Qty</th>
                                    <th style="width: 15%;">Unit Price</th>
                                    <th style="width: 15%;">Amount</th>
                                    <th style="width: 15%;">Action</th>
                                </tr>
                                </thead>
                                                  
                                <tbody>
                                    <?php if(!empty($orders)){ 
                                    foreach ($orders as $key => $value) { 
                                   	$counter = $key; ?>
                                                      
                                    <tr class='social_tr'>
                                                          
                                    <td>
                                        <input type="text" name="Checkout[<?php echo $key; ?>][product_name]" class="form-control product_name" value="<?php echo $value->name; ?>" />
                                        <input type="hidden" name="Checkout[<?php echo $key; ?>][product_id]" class="form-control product_id" value="<?php echo $value->product_id; ?>" />
                                        <input type="hidden" name="Checkout[<?php echo $key; ?>][cat_id]" class="form-control cat_id" value="<?php echo $value->cat_id; ?>" />
                                    </td>
                                                          
                                    <td><input type="number" name="Checkout[<?php echo $key; ?>][qty]" class="form-control qty" value="<?php echo $value->quantity; ?>" autocomplete="off" /></td>

                                    <td><input type="text" class="form-control unit_price" name="Checkout[<?php echo $key; ?>][unit_price]" value="<?php echo $value->price; ?>" autocomplete="off" /> </td>

                                    <td><input type="text" class="form-control amount" name="Checkout[<?php echo $key; ?>][amount]" value="<?php echo $value->price * $value->quantity; ?>" autocomplete="off" /> </td>
                                                          
                                    <td>
                                        <button class='add_social_tr btn btn-primary fa fa-plus' data-names=""></button>
                                        <button class='delete_expense_input_field btn btn-danger fa fa-trash-o'></button>
                                    </td>

                                    </tr>

                                    <?php } } ?>


                                <tr id="add_social_tr_before">
                                                      
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                                  
                                </tbody>
                                              
                            </table>
                            </div>

                      </div>
                    </div>

                    <div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 1%">
						<div class="form-group">
		                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Update', array('class'=>'btn btn-success')); ?>
		                </div>
		                </div>
		            </div>

                    <?php $this->endWidget(); ?>

                  </div>
              </div>
            </div>
          </div>

<script type="text/javascript">

var options = {
   source:"<?php echo Yii::app()->CreateUrl('Product/AutoCompProducts'); ?>", 
		minLength:2,
		select: function(event,ui){
        	$(this).closest('tr').find(".product_id").val(ui.item.id);
        	$(this).closest('tr').find(".cat_id").val(ui.item.cat_id);
			$(this).closest('tr').find(".unit_price").val(ui.item.price);
		},
};

var selector = 'input.product_name';
$(document).on('keydown.autocomplete', selector, function() {
    $(this).autocomplete(options);
});


$("body").on('focusout',".qty",function () {
    
    var unit_price = $(this).closest('tr').find(".unit_price").val();
    var qty = $(this).val();

    if(unit_price == null){
    	return false;
    }

    var amount = parseFloat(unit_price * qty);

    $(this).closest('tr').find(".amount").val(amount)

});


var counter = <?php echo $counter; ?>

$("body").on('click','.add_social_tr',function(e){

    counter++;

    e.preventDefault();

        name_attr = $(this).attr('data-names');
        
        var parent_parent_div = $(this).closest('.social_tr').parent();
        
        new_div ='';

		new_div +='<tr>';        
        
        new_div +='<td><input type="text" name="Checkout['+counter+'][product_name]" class="form-control product_name" /> <input type="hidden" name="Checkout['+counter+'][product_id]" class="form-control product_id" /> <input type="hidden" name="Checkout['+counter+'][cat_id]" class="form-control cat_id" /></td>';

        new_div +='<td><input type="number" class="form-control qty" name="Checkout['+counter+'][qty]" autocomplete="off" /></td>';

        new_div +='<td><input type="text" class="form-control unit_price" name="Checkout['+counter+'][unit_price]" autocomplete="off" /></td>';

        new_div +='<td><input type="text" class="form-control amount" name="Checkout['+counter+'][amount]" autocomplete="off" /></td>';

        new_div +='<td><button class="add_social_tr btn btn-primary fa fa-plus" data-names=""></button>';

        new_div +='<button class="delete_expense_input_field btn btn-danger fa fa-trash-o"></button></td>';

        new_div +='</tr>';


        $("#add_social_tr_before").before(new_div);

    });

    $("body").on('click','.delete_expense_input_field',function(e){
        
        e.preventDefault();

        parent_parent_div = $(this).closest('tr').remove();

});

</script>