<div class="row">
<div class="col-md-12 col-xs-12">
<div class="x_title">
       <h1>Manage Orders</h1>
<div class="clearfix"></div>
</div>
                  
<div class="x_content">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
	<label>From Date</label>
    <input type="text" <?php if(!empty($_GET) && !empty($_GET['date_from'])) echo "value ='".$_GET['date_from']."'";?> name='date_from' class="form-control datepicker1" autocomplete="off" />
	</div>

	<div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
	<label>To Date</label>
    <input type='text' <?php if(!empty($_GET) && !empty($_GET['date_to'])) echo "value ='".$_GET['date_to']."'";?> name='date_to' class="form-control datepicker1" autocomplete="off" />
	</div>

        
    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
        <?php echo $form->label($model,'receipt_no'); ?>
        <?php echo $form->textField($model,'receipt_no', array('size'=>60,'maxlength'=>256,'class'=>'form-control')); ?>
    </div>


    <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback" style="margin-top: 1%">
       	<?php echo $form->label($model,'order_type'); ?>
        <?php $temp_list =  array(''=>'-- Please Select --', 'Dine in'=>'Dine in' ,'Takeaway'=>'Takeaway','Delivery'=>'Delivery'); ?>
		<?php echo $form->dropDownList($model,'order_type', $temp_list, array('class'=>'form-control')); ?>
    </div>


    <div class="form-group">
    <div class="col-md-10 col-sm-9 col-xs-12">
        <?php echo CHtml::submitButton('Search',array('class'=>'btn btn-success')); ?>
    </div>
    </div>

<?php $this->endWidget(); ?>
</div>

</div>
</div>

<script>
 $(function() {
   
//datepicker1 is the class given to element
	
	 $(document).on('focusin', '.datepicker1', function(){
      $(this).datepicker({
		  format: 'yyyy-mm-dd',
		  changeMonth: true,
		  changeYear: true,
		  yearRange: '1900:<?php echo date('Y')?>'
		});
    });
	
  });
</script>