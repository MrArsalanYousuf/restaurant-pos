<?php

$this->breadcrumbs=array(
	'Checkouts'=>array('index'),
	$model->id,
);

?>

        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
              	<div class="x_title">
                  <h2>Order Information</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <section class="content invoice">
                    <div class="row">
                      <div class="col-xs-12 invoice-header">
                        <h1>
                            Receipt No. <?php echo $model->receipt_no; ?>
                            <small class="pull-right">Date: <?php echo date('Y-m-d H:i:s', strtotime($model->date_added)); ?></small>
                        </h1>
                      </div>
                    </div>
                    
                    <div class="row invoice-info">
                    
                      <div class="col-md-4 col-sm-12 col-xs-12 invoice-col">
                        
                        <h1 style="margin-bottom: 10px;color: red"><?php echo $model->order_type; ?></h1>
                        
                      </div>
                     
                  	</div>


                    <?php if($model->order_type == "Delivery"){ 

                    	$address = Address::model()->find(array("condition"=>"checkout_id = $model->id"));

                    	if(!empty($address)){

                    ?>

                  	<div class="row invoice-info">

                    	<div class="col-md-4 col-sm-12 col-xs-12 invoice-col">
                        
                        <p>Customer Details:</p>
                        
                        <address>
                            <strong><?php echo $address->name; ?></strong>
                            <br>
                            <strong><?php echo $address->number; ?></strong>
                            <br>
                            <strong><?php echo $address->address; ?></strong>
                        </address>
                      </div>
                     
                  	</div>

                  <?php } } ?>

                  	<hr>

                  	<?php $orders = OrderProduct::model()->findAll(array("condition"=>"checkout_id = $model->id")); ?>

                    <!-- Table row -->
                    <div class="row">
                      <div class="col-xs-12 table">
                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>Qty</th>
                              <th style="width: 59%">Product</th>
                              <th>Unit Price</th>
                              <th>Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                          	
                          <?php foreach ($orders as $k => $v) { ?>
                            
                            <tr>
                              <td><?php echo $v->quantity; ?></td>
                              <td><?php echo $v->name; ?></td>
                              <td><?php echo "PKR ".$v->price; ?></td>
                              <td><?php echo "PKR ".($v->price * $v->quantity); ?></td>
                            </tr>

                        	<?php } ?>

                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-6 pull-right">
                        <div class="table-responsive">
                          <table class="table table-bordered table-hover">
                            <tbody>
                              <tr>
                                <th>Subtotal:</th>
                                <td><?php echo "PKR ". $model->subtotal; ?></td>
                              </tr>
                              <tr>
                                <th>Discount (0%)</th>
                                <td>PKR 0</td>
                              </tr>
                              <tr>
                                <th>Net Total</th>
                                <td><?php echo "PKR ". $model->subtotal; ?></td>
                              </tr>
                              <tr>
                                <th>GST (13%)</th>
                                <td>PKR 0</td>
                              </tr>
                              <tr>
                                <th>Grand Total:</th>
                                <td><?php echo "PKR ". $model->total; ?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                      <div class="col-xs-12">
                        <a href="<?php echo $this->CreateUrl('Checkout/Slip/'.$model->id); ?>" class="btn btn-success pull-right" data-toggle="tooltip" data-placement="top" title="Print Slip"><i class="fa fa-print"></i> Print</a>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>