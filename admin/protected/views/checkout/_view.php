<?php $orders = OrderProduct::model()->findAll(array("condition"=>"checkout_id = $data->id")); ?>

<tr>
	
    <td>
		<?php echo date('d-M-Y H:i:s', strtotime($data->date_added)); ?>
    </td>
	 
	<td>
		<strong><?php echo $data->receipt_no; ?></strong>
	</td>

	<td>
		<strong style="color: red"><?php echo $data->order_type; ?></strong>
	</td>

	<td style="font-size:12px">
		<strong style="color: blue"><?php 
				if(!empty($orders)){
					foreach($orders as $k => $order){
						$k++;
						echo $k.") ".$order->name." (".$order->quantity.")".'<br>';
					}
				}
		?></strong>
	</td>


	<td>
		<?php echo "PKR ".number_format($data->subtotal, 2); ?>
	</td>

	<td>
		<?php echo "PKR ".number_format($data->total, 2); ?>
	</td>

	<td>
		
		<a href="<?php echo $this->CreateUrl('Checkout/Update/'.$data->id); ?>" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>

		<a href="<?php echo $this->CreateUrl('Checkout/View/'.$data->id); ?>" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>

		<a href="<?php echo $this->CreateUrl('Checkout/Slip/'.$data->id); ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Print Slip"><i class="fa fa-print"></i></a>

	</td>
</tr>