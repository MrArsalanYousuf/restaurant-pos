<?php

$this->breadcrumbs=array(
	'Checkouts'=>array('index'),
	$model->id,
);

?>

<style type="text/css">
@media print {

    div.break {page-break-after: always;}

}

.table>tbody>tr>td, .table>tbody>tr>th {
    border-top: none !important;
}

.separator-start{
  height: 20px;
}

.separator-end{
  height: 20px;
}
</style>

<?php $acc = Accessories::model()->find(array("condition"=>"1=1 ORDER BY id DESC"));
$logo = $acc->logo;
?>

        <div class="container">
          <div class="row">
            <div class="col-md-12">
              
              <div class="x_panel" id="printableArea">

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                <div class="row">

                <div class="col-md-12 col-xs-12">

                  <img src="<?php echo Yii::app()->baseUrl .'/images/logo/'.$logo; ?>" alt="<?php echo $acc->title; ?>" class="img-responsive center-block">

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                  <h5 style="text-transform: uppercase;"><?php echo $acc->address; ?></h5>
                  <h5>PH: <?php echo $acc->phone_no; ?> &nbsp;&nbsp; <?php echo $acc->phone_no; ?></h5>

                  <br>

                  <h5><?php echo date('d-M-Y h:i A', strtotime($model->date_added)); ?></h5>
                  <h5 style="text-transform: uppercase;font-weight: 600"><?php echo $model->order_type; ?></h5>
                  <h4 style="font-weight: 600;font-size: 22px">Receipt No. <?php echo $model->receipt_no; ?></h4>
                  <h5>Original Receipt</h5>

                </div>


                </div> <!-- row -->


                <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                  <table id="datatable" class="table">
                    
                    <thead class="black white-text">

                        <tr>

                            <th class="text-left" scope="col" style="width:10%;">
                              Qty
                            </th>

                            <th scope="col" style="width:50%;">
                              Particulars
                            </th>

                            <th class="text-right" scope="col" style="width:20%;">
                              Rate
                            </th>

                            <th class="text-right" scope="col" style="width:20%;">
                              Amount
                            </th>

                        </tr>
                      </thead>

                  <?php $orders = OrderProduct::model()->findAll(array("condition"=>"checkout_id = $model->id")); ?>

                    <tbody class='Personal_info-tbody'>

                    <tr class="separator-start" colspan="4"></tr>

                        <?php if(!empty($orders)){
                        foreach ($orders as $k => $v) { ?>
                            
                            <tr>
                              <td class="text-left"><?php echo $v->quantity; ?></td>
                              <td><?php echo $v->name; ?></td>
                              <td class="text-right"><?php echo number_format($v->price, 2); ?></td>
                              <?php $amount = $v->price * $v->quantity; ?>
                              <td class="text-right"><?php echo number_format($amount, 2); ?></td>
                            </tr>

                        <?php } } ?>

                    <tr class="separator-end" colspan="4"></tr>

                    </tbody>

                    <tbody class='Personal_info-tbody' style="border-bottom: 2px solid #ddd">

                      <tr>
                        <th colspan="2">Sub Total:</th>
                        <td class="text-right" class="text-right" colspan="2">PKR <strong><?php echo number_format($model->subtotal, 2); ?></strong></td>
                      </tr>

                      <tr>
                        <th colspan="2">Dis 0%:</th>
                        <td class="text-right" colspan="2">PKR <strong>0.00</strong></td>
                      </tr>

                      <tr>
                        <th colspan="2">Net Total:</th>
                        <td class="text-right" colspan="2">PKR <strong><?php echo number_format($model->subtotal, 2); ?></strong></td>
                      </tr>

                      <tr>
                        <th colspan="2">GST 13%:</th>
                        <td class="text-right" colspan="2">PKR <strong>0.00</strong></td>
                      </tr>

                      <tr>
                        <th colspan="2">Grand Total:</th>
                        <td class="text-right" colspan="2">PKR <strong><?php echo number_format($model->total, 2); ?></strong></td>
                      </tr>

                    </tbody>
                                    
                  </table>

                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                  <h5>Thank you for your visit</h5>

                </div>

                <hr>

                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                  <h5>Powered by Arsalan Yousuf</h5>
                  <p>+923201251328</p>

                </div>

                </div><!-- row 2 -->

              </div> 

              </div>

              <div class='break'></div>

            </div>
          </div>
        </div>

<?php

if (!headers_sent()) {

    header('Refresh:0;url='. $this->createUrl('Checkout/admin'));

} ?>   

<script type="text/javascript">
$(document).ready(function() {

    <?php
  
    $data['hide_element_classes'] = "left_col,right_col,top_nav,falsh,print_hide,footer,pull-right";
    $data['printable_area_id'] = "printableArea";
    
    $this->renderPartial('application.components.danish.print_script',array(
        'data'=>$data,
    ));

    ?>

});
</script>