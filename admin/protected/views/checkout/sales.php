<div class="search-form">
<?php require('sales_filter.php'); ?>
</div><!-- search-form -->


<?php
$acc = Accessories::model()->find(array("condition"=>"1=1 ORDER BY id DESC"));
$logo = $acc->logo;

$date_from = (isset($_GET['date_search_from']) && !empty($_GET['date_search_from']) ? date('Y-m-d',strtotime($_GET['date_search_from'])) : date('Y-m-01'));
$date_to = (isset($_GET['date_search_to']) && !empty($_GET['date_search_to']) ? date('Y-m-d',strtotime($_GET['date_search_to'])) : date('Y-m-t'));
?>



<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  	
					<ul class="nav navbar-right panel_toolbox">

						<li style="margin-top: 5px">
							<strong style="color: #000">items per page</strong>&nbsp;&nbsp;
						</li>

						<li>
						<form method="get" id="limit_form" action="<?php echo $this->createUrl('Checkout/Sales'); ?>">
						<input type="hidden" class="form-control" name="date_search_from" value="<?php echo $date_from; ?>" />
						<input type="hidden" class="form-control" name="date_search_to" value="<?php echo $date_to; ?>" />
		                    <select class='form-control changenumber pull-right' name="limit">
		                    	<option value='5'>5</option>
		    					<option value='10' selected="true">10</option>
		    					<option value='25'>25</option>
		    					<option value='50'>50</option>
		    					<option value='100'>100</option>
						    </select>
						</form>
						</li>

					</ul>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="table-responsive" id="printableArea">

		            <div class="row sales-header-details" style="display: none">

		                <div class="col-md-12 col-xs-12">

		                  <img src="<?php echo Yii::app()->baseUrl .'/images/logo/'.$logo; ?>" alt="<?php echo $acc->title; ?>" class="img-responsive center-block">

		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

		                  <h5 style="text-transform: uppercase;color: #000;"><?php echo $acc->address; ?></h5>
		                  <h5 style="color: #000;">PH: <?php echo $acc->phone_no; ?> &nbsp;&nbsp; <?php echo $acc->phone_no; ?></h5>

		                  <br>

		                  <h5 style="color: #000"><?php echo date('d-M-Y h:i A'); ?></h5>
                  		  <h4 style="font-weight: 600;font-size: 22px;color: #000">Sales Summary</h4>
		                  <span style="color: #000"><strong>FROM: &nbsp;<?php echo ($date_from ? date('d-m-Y',strtotime($date_from)) : '-'); ?>&nbsp;&nbsp;&nbsp;TO: &nbsp;<?php echo($date_to ? date('d-m-Y',strtotime($date_to)) : '-'); ?></strong></span>

		                </div>

		            </div> <!-- row -->

		            <br/>

                    <table id="datatable" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>

						<th scope="col">
							Date & Time
						</th>

						<th scope="col">
							Total Orders
						</th>

						<th scope="col">
							Gross Amount
						</th>

						<th scope="col">
							Discount 0%
						</th>

						<th scope="col">
							Net Amount
						</th>

						<th scope="col">
							GST 0%
						</th>

						<th scope="col">
							Total Amount
						</th>

                    </tr>
                  </thead>
                  <tbody>
                  	    
                  	    <?php if(!empty($checkout)){
							foreach($checkout as $key => $value){
								$date = date('Y-m-d', strtotime($value->date_added));
								//print_r($date);die();
							?>
								<tr>
								
									<td><?php echo date('M, d, Y, l', strtotime($date)); ?></td>
									
									<?php $ordersCounter = Checkout::model()->count(array("condition"=>"DATE(`date_added`) = '$date'")); ?>
									<td><strong style="color: blue"><?php echo $ordersCounter; ?></strong></td>

									<?php $gross_total = "Select SUM(subtotal) AS grosstotal from checkout where DATE(`date_added`) = '$date'"; ?>	
									<?php $gross_total = Yii::app()->db->createCommand($gross_total)->queryAll();?>
									<?php $gross_total = $gross_total[0]['grosstotal']; ?>


									<?php $total_sales = " Select SUM(total) AS total from checkout where DATE(`date_added`) = '$date'"; ?>	
									<?php $total_sales = Yii::app()->db->createCommand($total_sales)->queryAll(); ?>
									<?php $total_sales = ($total_sales[0]['total'] ? $total_sales[0]['total'] : 0); ?>

									<td><?php echo "PKR ".number_format($gross_total, 2); ?></td>
									
									<td><?php echo "PKR ".number_format(0, 2); ?></td>
									
									<td><?php echo "PKR ".number_format($gross_total, 2); ?></td>
									
									<td><?php echo "PKR ".number_format(0, 2); ?></td>
									
									<td><strong style="color: red"><?php echo "PKR ".number_format($total_sales, 2); ?></strong></td>
								
								</tr>

						<?php } } ?>
                      	
                  </tbody>
                    </table>

                    <br/>

                    <div class='div-pagination Personal_info print_hide'>
						<?php $this->widget('CLinkPager', array(
						    'pages' => $pages,
						)) ?>
                    </div>

                    <br/>

                    <div class="row sales-footer-details" style="display: none">

                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                  			<h5>Powered by Arsalan Yousuf</h5>
                  			<p>+923201251328</p>

                		</div>

                	</div>

                    </div>
                  </div>
                </div>
              </div>
             </div>
           </div>


<style>
.div-pagination.Personal_info{
	text-align:center;
}
	
.yiiPager>li>a {
	position: relative;
	padding: 10px 15px !important;
	color: #3b9c96 !important;
	text-decoration: none !important;
	background-color: #fff !important;
	border: 1px solid #ddd !important;
	border-radius: 2px !important;
}

ul.yiiPager .selected a {
	color:#fff !important;
	background-color:#3b9c96 !important;
	font-weight: 800;
}

.pager .next>a, .pager .next>span {
	float:none;
}

.pager .previous>a, .pager .previous>span {
	float:none;
}
</style>

<script>
$(".changenumber").change(function(){

	var changenumber = $(this).val();
	if(changenumber == ''){
		alert('Value Cannot Be Empty');
		return 0;
	}
	$('form#limit_form').submit();
});

$("#print_out_this_page").click(function() {

	$(".sales-header-details").css("display","block");
	$(".sales-footer-details").css("display","block");

    <?php
  
    $data['hide_element_classes'] = "left_col,right_col,top_nav,falsh,print_hide,footer,pull-right";
    $data['printable_area_id'] = "printableArea";
    
    $this->renderPartial('application.components.danish.print_script',array(
        'data'=>$data,
    ));

    ?>

    $(".sales-header-details").css("display","none");
    $(".sales-footer-details").css("display","none");

});
</script>