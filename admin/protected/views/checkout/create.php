<?php
/* @var $this CheckoutController */
/* @var $model Checkout */

$this->breadcrumbs=array(
	'Checkouts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Checkout', 'url'=>array('index')),
	array('label'=>'Manage Checkout', 'url'=>array('admin')),
);
?>

<h1>Create Checkout</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>