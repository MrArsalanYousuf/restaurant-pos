<?php

/**
 * This is the model class for table "accessories".
 *
 * The followings are the available columns in table 'accessories':
 * @property integer $id
 * @property string $logo
 * @property string $favi_image
 * @property string $title
 * @property string $footer_name
 * @property string $date_added
 */
class Accessories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'accessories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('logo, title, footer_name, phone_no, mobile_no, address', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('discount, tax', 'numerical'),
			array('type', 'length', 'max'=>20),
			array('title, footer_name, email, phone_no, mobile_no, address', 'length', 'max'=>256),
			array('id, logo, favi_image, email, title, footer_name, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'logo' => 'Logo',
			'favi_image' => 'Favi Image',
			'title' => 'Title',
			'email' => 'Email',
			'phone_no' => 'Phone No.',
			'mobile_no' => 'Mobile No.',
			'footer_name' => 'Footer Name',
			'discount' => 'Discount',
			'tax' => 'Tax in Percentage',
			'type' => 'Discount Type',
			'date_added' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('phone_no',$this->phone_no);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('favi_image',$this->favi_image,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('footer_name',$this->footer_name,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Accessories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
