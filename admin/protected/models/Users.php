<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $image
 * @property string $user_type
 * @property integer $created_by
 * @property integer $status
 * @property string $date_added
 * @property string $date_modified
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, first_name, last_name, password, email, user_type, created_by, date_added, date_modified', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('username, first_name, last_name, email, status', 'length', 'max'=>256),
			array('user_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, first_name, last_name, password, email, image, user_type, created_by, status, date_added, date_modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'password' => 'Password',
			'email' => 'Email',
			'image' => 'Image',
			'user_type' => 'User Type',
			'created_by' => 'Created By',
			'status' => 'Status',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('image',$this->image,true);
		//$criteria->compare('user_type','employee');
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->addCondition('user_type != "admin" ');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// public function addemployee()
	// {
		//@todo Please modify the following code to remove attributes that should not be searched.

		// $criteria=new CDbCriteria;

		// $criteria->compare('id',$this->id);
		// $criteria->compare('username',$this->username,true);
		// $criteria->compare('first_name',$this->first_name,true);
		// $criteria->compare('last_name',$this->last_name,true);
		// $criteria->compare('password',$this->password,true);
		// $criteria->compare('email',$this->email,true);
		// $criteria->compare('image',$this->image,true);
		// $criteria->compare('user_type','employee');
		// $criteria->compare('created_by',$this->created_by);
		// $criteria->compare('status',$this->status);
		// $criteria->compare('date_added',$this->date_added,true);
		// $criteria->compare('date_modified',$this->date_modified,true);

		// return new CActiveDataProvider($this, array(
			// 'criteria'=>$criteria,
		// ));
	// }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
