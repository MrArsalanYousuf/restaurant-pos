<?php

/**
 * This is the model class for table "checkout".
 *
 * The followings are the available columns in table 'checkout':
 * @property string $id
 * @property string $invoice_no
 * @property string $order_type
 * @property string $invoice_prefix
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $address
 * @property integer $city_id
 * @property string $country
 * @property integer $order_status
 * @property double $total
 * @property double $net_amount
 * @property string $products
 * @property string $ip
 * @property string $user_id
 * @property string $date_added
 * @property string $date_modified
 * @property double $discount
 * @property double $tax
 */
class Checkout extends CActiveRecord
{

	public function tableName()
	{
		return 'checkout';
	}

	public function rules()
	{
		return array(
			array('receipt_no, order_type, order_status, subtotal, total', 'required'),
			array('order_status, user_id', 'numerical', 'integerOnly'=>true),
			array('subtotal, total, net_amount, discount, tax', 'numerical'),
			array('receipt_no, order_type, ip', 'length', 'max'=>50),
			array('id, receipt_no, order_type, order_status, subtotal, total, net_amount, ip, user_id, date_added, date_modified, discount, tax', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'receipt_no' => 'Receipt No',
			'order_type' => 'Order Nature',
			'order_status' => 'Order Status',
			'subtotal' => 'Subtotal',
			'total' => 'Total',
			'net_amount' => 'Net Amount',
			'ip' => 'Ip',
			'user_id' => 'User',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
			'discount' => 'Discount',
			'tax' => 'Tax',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('receipt_no',$this->receipt_no,true);
		$criteria->compare('order_type',$this->order_type,true);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('subtotal',$this->subtotal);
		$criteria->compare('total',$this->total);
		$criteria->compare('net_amount',$this->net_amount);
		$criteria->compare('products',$this->products,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('tax',$this->tax);
		$criteria->order = "t.id DESC";

        $date_to = Yii::app()->request->getQuery('date_to');
		$date_from = Yii::app()->request->getQuery('date_from');
		
		if(!empty($date_from) && !empty($date_to)){
			
			$criteria->addCondition('DATE(t.date_added) >= :from');
			$criteria->params[':from']= $date_from;
			
			$criteria->addCondition('DATE(t.date_added) <= :to');
			$criteria->params[':to']= $date_to;
			
		}else if(!empty($date_from)){
			$criteria->addCondition('DATE(t.date_added) >= :from');
			$criteria->params[':from']= $date_from;
			
		}else if(!empty($date_to)){
			$criteria->addCondition('DATE(t.date_added) <= :to');
			$criteria->params[':to']= $date_to;
		}
        

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
			'pageSize'=>!empty($page_size)?$page_size:20,
			),   
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
