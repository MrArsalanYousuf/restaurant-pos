<?php

class Product extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, name, price', 'required'),
			array('user_id, status, cat_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('name, code, size', 'length', 'max'=>256),
			array('image, description', 'safe'),
			array('id, cat_id, name, code, price, size, description, user_id, image, status, date_added, date_modified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'category'=>array(self::BELONGS_TO, 'Category', '', 'foreignKey' => array('cat_id'=>'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cat_id' => 'Category',
			'name' => 'Name',
			'code' => 'Code',
			'price' => 'Price',
			'size' => 'Size',
			'description' => 'Description',
			'user_id' => 'User',
			'image' => 'Image',
			'status' => 'Status',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('cat_id',$this->cat_id,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('size',$this->size,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('t.status',1);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->order = "t.id DESC";

		$criteria->with = array(
			'category'=>array('select'=>'name')
		);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
