<?php

class Category extends CActiveRecord
{
	
	public function tableName()
	{
		return 'category';
	}


	public function rules()
	{

		return array(
			array('name', 'required'),
			array('priority', 'numerical', 'integerOnly'=>true),
			array('name, description', 'length', 'max'=>256),
			array('id, name, description, status, priority, date_added', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'status' => 'Status',
			'description' => 'Description',
			'priority' => 'Priority',
			'date_added' => 'Date Added',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->name,true);
		$criteria->compare('priority',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
