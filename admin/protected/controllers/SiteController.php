<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		
		if(Yii::app()->user->isGuest){
                $this->redirect(array('site/login'));
		}
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionChatApplication()
	{
		$this->render('chat');
	}
    
	public function actionChat()
	{
	  $user_id = $_POST['user_id'];
	  $msg = $_POST['msg'];
	  if(!empty($user_id) || !empty($msg)){
	  $chat =new Chat;
	  $chat->to = $user_id;
	  $chat->from = Yii::app()->user->id;
	  $chat->message = $msg;
	  $chat->date_added = date('Y-m-d H:i:s');
	  $chat->save();
	  
		 
	 
	  }
	   //print_r($msg);die();
	}
	
	public function actionShowMessage(){
		$id = Yii::app()->user->id;
		$ChatData = Chat::model()->findAll();
		foreach($ChatData as $ShowMsg){
			echo $ShowMsg->message;
		}
		
		  
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid

			Yii::app()->session['logged_in_user'] = isset($_POST['LoginForm']['username'])? $_POST['LoginForm']['username']:'';
			Yii::app()->session['logged_in_user'] = isset($_POST['LoginForm']['password'])? $_POST['LoginForm']['password']:'';


			if($model->validate() && $model->login()){
				// For Mobile Application Only

				if (isset($_POST['tourist_key'])) {
						if ($_POST['tourist_key'] == 'yest') {
								$json = array();
								$row = Yii::app()->session['logged_in_user'];

								$json['user']['user_id'] = $row->api_key;
								$json['user']['f_name'] = $row->f_name;
								$json['user']['l_name'] = $row->l_name;

								echo json_encode($json);
								die();
						}
				}



				$this->redirect(Yii::app()->user->returnUrl);
			}
			else{

			}
		}

		// display the login form
		$this->renderPartial('login_now',array('model'=>$model));
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('login'));
	}

	public function actionLoginCheck()
	{
		$model=new LoginForm;
		
		if(Yii::app()->request->isAjaxRequest) 
		{
			
		$model->attributes=$_POST;
		if($model->validate() && $model->login()){
			
			echo "Success";
		}else{
			
			echo Yii::app()->session['Error'] ;
			
		}
		}
	}
}