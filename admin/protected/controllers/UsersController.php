<?php

class UsersController extends Controller
{
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','DeactivateUser','ActivateUser','AddEmployee','CreateEmployee','UpdateEmployee','CompanyCreate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			$password = $this->better_crypt($model->password);
			$model->password = $password;
			$model->status = 1;
			$date_added = date('Y-m-d H:i:s');
			$model->date_added = $date_added;
			$model->date_modified = $date_added;
			$model->created_by = Yii::app()->user->id;
			$model->admin_id = (Yii::app()->user->id ? Yii::app()->user->id : 0 );

			$image = CUploadedFile::getInstance($model,'image');
			if($image !== null) {
				$image_new_name = time().$image->name;
			}
			else {
					$image_new_name =null;
			}
			$model->image = $image_new_name;

			if($model->save()){

				$path = Yii::app()->basePath.'/../images/users';
				if($image !== null) {
				$image->saveAs($path.'/'.$image_new_name);
	
				Yii::app()->user->setFlash('success',
						"<strong>
						<i class='icon-ok'></i>
						Users has been created successfully!	
					   </strong>");
					
				$model=new Users;
			}
		}
	}
	$this->render('create',array(
			'model'=>$model,
		));
}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$prevImage=$model->image;

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];

			$imageUser=CUploadedFile::getInstance($model,'image');
			
			if($imageUser !== null) {
				$image_name= time().$imageUser->name;
				$model->image = $image_name;
				$path = Yii::app()->basePath.'/../images/users/' .$model->image;
				$imageUser->saveAs($path);
			}else{
				$model->image=$prevImage;
			}

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionCreateEmployee()
	{
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			$password = $this->better_crypt($model->password);
			$model->password = $password;
			$model->status = 1;
			$date_added = date('Y-m-d H:i:s');
			$model->date_added = $date_added;
			$model->date_modified = $date_added;
			$model->created_by = Yii::app()->user->id;
		    $model->user_type = 'employee';

			$image = CUploadedFile::getInstance($model,'image');
			if($image !== null) {
				$image_new_name = time().$image->name;
			}
			else {
					$image_new_name =null;
			}
			$model->image = $image_new_name;

			if($model->save()){

				$path = Yii::app()->basePath.'/../images/employee';
				if($image !== null) {
				$image->saveAs($path.'/'.$image_new_name);
	
				Yii::app()->user->setFlash('success',
						"<strong>
						<i class='icon-ok'></i>
						Employee has been created successfully!
						
					   </strong>");
					   $this->redirect(array('view','id'=>$model->id));
				$model=new Users;
			}
		}
	}
	$this->render('createemployee',array(
			'model'=>$model,
		));
}


public function actionUpdateEmployee($id)
	{
		$model=$this->loadModel($id);
		$prevImage=$model->image;

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];

			$imageUser=CUploadedFile::getInstance($model,'image');
			
			if($imageUser !== null) {
				$image_name= time().$imageUser->name;
				$model->image = $image_name;
				$path = Yii::app()->basePath.'/../images/employee/' .$model->image;
				$imageUser->saveAs($path);
			}else{
				$model->image=$prevImage;
			}

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('updateemployee',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
		public function actionAddEmployee()
	{
		$model=new Users('addemployee');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('addemployee',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function better_crypt($input, $rounds = 7)
	{
    $salt = "";
    $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
    for($i=0; $i < 22; $i++) {
      $salt .= $salt_chars[array_rand($salt_chars)];
    }
	
    return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
  }


	public function actionDeactivateUser(){
		$id = $_POST['id'];

		$Users = Users::model()->updateAll(array('status'=>'2'),"id = $id");

		if($Users){
			echo "success";
		}
		else{
			"fail";
		}
	}
	
	public function actionActivateUser(){
		$id = $_POST['id'];

		$Users = Users::model()->updateAll(array('status'=>'1'),"id = $id");

		if($Users){
			echo "success";
		}
		else{
			"fail";
		}
	}

}
