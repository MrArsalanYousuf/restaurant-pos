<?php

class CheckoutController extends Controller
{
	
	public $layout='//layouts/column2';

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','slip'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Sales','Items'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionSlip($id)
	{
		$this->render('slip',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate()
	{
		$model=new Checkout;

		if(isset($_POST['Checkout']))
		{

			$model->attributes=$_POST['Checkout'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Checkout']))
		{

			$date = ('Y-m-d H:i:s');
			$total = 0;

			if(!empty($_POST['Checkout'])){

				OrderProduct::model()->deleteAll(array('condition' => "checkout_id = $id"));

				foreach($_POST['Checkout'] as $k => $v){

					$total += $v['amount'];

					$product = new OrderProduct;
					$product->product_id = $v['product_id'];
					$product->cat_id = $v['cat_id'];
					$product->total = 0;
					$product->order_status = 0;
					$product->price = $v['unit_price'];
					$product->name = $v['product_name'];
					$product->tax = 0;
					$product->quantity = $v['qty'];
					$product->checkout_id = $id;
					$product->date_added = $date;
					$product->date_modified = $date;
							
					$product->save(false);
					
				}
			}

			Checkout::model()->updateAll(array('subtotal'=>$total,'total'=>$total)," id = $id ");
			OrderProduct::model()->updateAll(array('total'=>$total)," checkout_id = $id ");

			Yii::app()->user->setFlash('success',
			"<strong>
			<i class='icon-ok'></i>
			Order update successfully!
			</strong>");
			$this->redirect(array('Admin'));

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Checkout');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Checkout('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Checkout']))
			$model->attributes=$_GET['Checkout'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Checkout the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Checkout::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Checkout $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='checkout-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionItems()
    {
        $criteria = new CDbCriteria;

        $date_from = (isset($_GET['date_search_from']) ? date('Y-m-d',strtotime($_GET['date_search_from'])) : date('Y-m-01'));
        $date_to = (isset($_GET['date_search_to']) ? date('Y-m-d',strtotime($_GET['date_search_to'])) : date('Y-m-t'));
        $limit = (isset($_GET['limit']) && !empty($_GET['limit']) ? $_GET['limit'] : 10);

        if(!empty($date_from) && !empty($date_to)){

            $criteria->addCondition('DATE(t.date_added) >= :date_from');
            $criteria->params[':date_from'] = $date_from;

            $criteria->addCondition('DATE(t.date_added) <= :date_to');
            $criteria->params[':date_to'] = $date_to;

        }else if(!empty($date_from)){
            $criteria->addCondition('DATE(t.date_added) >= :date_from');
            $criteria->params[':date_from']= $date_from;

        }else if(!empty($date_to)){
            $criteria->addCondition('DATE(t.date_added) <= :date_to');
            $criteria->params[':date_to']= $date_to;
        }

        $criteria->group = "product_id";
        $criteria->order = "id DESC";


        $count = OrderProduct::model()->count($criteria);
    	$pages = new CPagination($count);

	    // results per page
	    $pages->pageSize = ($limit > 0 ? $limit : 10);
	    $pages->applyLimit($criteria);
        $items = OrderProduct::model()->findAll($criteria);

        $this->render('items', array(
            'items' => $items,
         	'pages' => $pages
        ));
    }

	public function actionSales()
    {
        $criteria = new CDbCriteria;

        $date_from = (isset($_GET['date_search_from']) ? date('Y-m-d',strtotime($_GET['date_search_from'])) : date('Y-m-01'));
        $date_to = (isset($_GET['date_search_to']) ? date('Y-m-d',strtotime($_GET['date_search_to'])) : date('Y-m-t'));
        $limit = (isset($_GET['limit']) && !empty($_GET['limit']) ? $_GET['limit'] : 10);

        if(!empty($date_from) && !empty($date_to)){

            $criteria->addCondition('DATE(t.date_added) >= :date_from');
            $criteria->params[':date_from'] = $date_from;

            $criteria->addCondition('DATE(t.date_added) <= :date_to');
            $criteria->params[':date_to'] = $date_to;

        }else if(!empty($date_from)){
            $criteria->addCondition('DATE(t.date_added) >= :date_from');
            $criteria->params[':date_from']= $date_from;

        }else if(!empty($date_to)){
            $criteria->addCondition('DATE(t.date_added) <= :date_to');
            $criteria->params[':date_to']= $date_to;
        }

        $criteria->group = "DATE(t.date_added)";
        $criteria->order = "id DESC";


        $count=Checkout::model()->count($criteria);
    	$pages=new CPagination($count);

	    // results per page
	    $pages->pageSize = ($limit > 0 ? $limit : 10);
	    $pages->applyLimit($criteria);
        $checkout = Checkout::model()->findAll($criteria);

        $this->render('sales', array(
            'checkout' => $checkout,
         	'pages' => $pages
        ));
    }

}
