<?php

class AccessoriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Accessories;

		$model->type = 'percentage';
		
		if(isset($_POST['Accessories']))
		{
			$model->attributes=$_POST['Accessories'];
			
			$image_logo = CUploadedFile::getInstance($model,'logo');
			$image_favicon = CUploadedFile::getInstance($model,'favi_image');
			
			if($image_logo !== null) {
				$image_new_name1 = time().$image_logo->name;
			}
			else {
				$image_new_name1 =null;
			}
			$model->logo = $image_new_name1;
			
			
			if($image_favicon !== null) {
				$image_new_name2 = time().$image_favicon->name;
			}
			else {
				$image_new_name2 =null;
			}
			$model->favi_image = $image_new_name2;
			
			$date =date('Y-m-d H:i:s');
			$model->date_added = $date;
			if($model->save())
			{
				$path = Yii::app()->basePath.'/../images/logo/'.$image_new_name1;
				
				if($image_logo !== null) {
					$image_logo->saveAs($path);
				}

				$p = Yii::app()->basePath.'/../images/logo/'.$image_new_name2;
				
				if($image_favicon !== null) {
					$image_favicon->saveAs($p);	
				}

				$this->redirect('admin');
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$prevImage_logo=$model->logo;
		$prevImage=$model->favi_image;

		if(isset($_POST['Accessories']))
		{
			$model->attributes=$_POST['Accessories'];

			if(!empty($_FILES['Accessories']['tmp_name']['logo'])){

				$fileinfo = getimagesize($_FILES['Accessories']['tmp_name']['logo']);
				$width = $fileinfo[0];
				$height = $fileinfo[1];

				$allowed_image_extension = array(
					"png"
				);
				
				// Get image file extension
				$file_extension = pathinfo($_FILES['Accessories']['name']['logo'], PATHINFO_EXTENSION);

				if (! in_array($file_extension, $allowed_image_extension)) {

					Yii::app()->user->setFlash('danger',
						"<strong>
						<i class='icon-ban'></i>
						Please upload valid images. Only PNG are allowed.
						</strong>");

					$this->redirect(array('admin'));

				}
				else if (($_FILES['Accessories']['size']['logo'] > 2000000)) {

					Yii::app()->user->setFlash('danger',
						"<strong>
						<i class='icon-ban'></i>
						Image size exceeds 2MB
						</strong>");

					$this->redirect(array('admin'));

				}
				else if ($width > "250" || $height > "100") {

					Yii::app()->user->setFlash('danger',
						"<strong>
						<i class='icon-ban'></i>
						Image dimension should be within 250X100
						</strong>");

					$this->redirect(array('admin'));

				}

			}

			$image_logo = CUploadedFile::getInstance($model,'logo');

			if($image_logo !== null) {
				$image_new_name1 = time().$image_logo->name;

			}
			else {
				$image_new_name1 = $prevImage_logo;
			}

			$model->logo = $image_new_name1;

			$image_favicon = CUploadedFile::getInstance($model,'favi_image');

			if($image_favicon !== null) {
				$image_new_name2 = time().$image_favicon->name;
			}
			else {
				$image_new_name2 = $prevImage;
			}

			$model->favi_image = $image_new_name2;
			if($model->save())
			{
				$path = Yii::app()->basePath.'/../images/logo/'.$image_new_name1;

				if($image_logo !== null) {
					$image_logo->saveAs($path);

				}
				$p = Yii::app()->basePath.'/../images/logo/'.$image_new_name2;

				if($image_favicon !== null) {
					$image_favicon->saveAs($p);

				}

				Yii::app()->user->setFlash('info',
					"<strong>
					<i class='icon-check'></i>
					Display Settings Updated Successfully
					</strong>");

				$this->redirect(array('admin'));
			}

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Accessories');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Accessories('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Accessories']))
			$model->attributes=$_GET['Accessories'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Accessories the loaded model
	 * @throws CHttpException
	 */


	public function loadModel($id)
	{
		$model=Accessories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Accessories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='accessories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
