<?php

class ProductController extends Controller
{

	public $layout='//layouts/column2';


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','SuspendCategory','ActiveCategory','AutoCompProducts'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionCreate()
	{
		$model = new Product;

		$model->size = "No";

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];

			$date = date('Y-m-d H:i:s');
			$model->date_added = $date;
			$model->date_modified = $date;
			$model->status = 1;
			$model->user_id = (Yii::app()->user->id ? Yii::app()->user->id : 0);

			//Image Upload
			$image = CUploadedFile::getInstance($model,'image');
			if($image !== null) {
				$image_new_name = time().$image->name;
			}
			else {
				$image_new_name = null;
			}
			$model->image = $image_new_name;

			if($model->save())
			{

				if($model->size == "Yes"){

					if(isset($_POST['Product'][0]) && !empty($_POST['Product'][0]['size'])){
						
						foreach($_POST['Product'] as $key => $Product){

							if(empty($_POST['Product'][$key]['name'])){
								continue;
							}

							$Products = $_POST['Product'][$key];

							$name = trim($Products['name']);
							$price = trim($Products['price']);
							$size = trim($Products['size']);
								
							$ProductSize = new Product;
							$ProductSize->cat_id = $model->cat_id;
							$ProductSize->product_id = $model->id; 
							$ProductSize->name = $name;
							$ProductSize->price = $price;
							$ProductSize->size = $size;
							$ProductSize->user_id = (Yii::app()->user->id ? Yii::app()->user->id : 0);
							$ProductSize->image = $model->image;
							$ProductSize->status = 2;
							$ProductSize->date_added = $date;
							$ProductSize->date_modified = $date;

							$ProductSize->save(false);
						}
				  }
			}

			$path = Yii::app()->basePath.'/../images/products';
				if($image !== null) {	
					$image->saveAs($path.'/'.$image_new_name);
				}

				Yii::app()->user->setFlash('info',
				"<strong>
				<i class='icon-ban'></i>
					Added Successfully!
				</strong>");

				$model = new Product;
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$prev_image = $model->image;

		if(isset($_POST['Product']))
		{

			Product::model()->deleteAll(array("condition"=>"product_id = $id"));

			$model->attributes=$_POST['Product'];

			//Image Upload
			$image = CUploadedFile::getInstance($model,'image');
			if($image !== null) {
				$image_new_name = time().$image->name;
			}
			else {
				$image_new_name = $prev_image;
			}
			$model->image = $image_new_name;

			$date = date('Y-m-d H:i:s');

			if($model->save())
			{
				if($model->size == "Yes"){

					if(isset($_POST['Product'][0]) && !empty($_POST['Product'][0]['size'])){
						
						foreach($_POST['Product'] as $key => $Product){

							if(empty($_POST['Product'][$key]['name'])){
								continue;
							}

							$Products = $_POST['Product'][$key];

							$name = trim($Products['name']);
							$price = trim($Products['price']);
							$size = trim($Products['size']);
								
							$ProductSize = new Product;
							$ProductSize->cat_id = $model->cat_id;
							$ProductSize->product_id = $model->id; 
							$ProductSize->name = $name;
							$ProductSize->price = $price;
							$ProductSize->size = $size;
							$ProductSize->user_id = (Yii::app()->user->id ? Yii::app()->user->id : 0);
							$ProductSize->image = $model->image;
							$ProductSize->status = 2;
							$ProductSize->date_added = $date;
							$ProductSize->date_modified = $date;

							$ProductSize->save(false);
						}
				  }
			}

				$path = Yii::app()->basePath.'/../images/products';
				if($image !== null) {	
					$image->saveAs($path.'/'.$image_new_name);
				}

				Yii::app()->user->setFlash('info',
				"<strong>
				<i class='icon-ban'></i>
					Updated Successfully!
				</strong>");
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Product');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	public function actionAdmin()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionSuspendCategory(){
		$suspend_id = $_POST['suspend_id'];

        $pro = Product::model()->updateAll(array('status'=>'0')," id = $suspend_id ");

		if($pro){
			echo "success";
		}
		else{
			"fail";
		}
	}

	public function actionActiveCategory(){
		$active_id = $_POST['active_id'];

        $pro = Product::model()->updateAll(array('status'=>'1')," id = $active_id ");

		if($pro){
			echo "success";
		}
		else{
			"fail";
		}
	}


	public function actionAutoCompProducts(){
		
		$searched_word = $_GET['term'];
		
		$match = addcslashes($searched_word, '%_'); // escape LIKE's special characters
		
		$q = new CDbCriteria( array(
				'condition' => "name LIKE :match AND status = 1 LIMIT 10",         // no quotes around :match
				'params'    => array(':match' => "$match%")  // Aha! Wildcards go here
		) );
		
		$rows = Product::model()->findAll($q);
		 
		$json_data = array();
		$arr = array();
		
		foreach($rows as $row){
           			
			$arr['label'] = htmlentities(stripslashes($row->name));
			$arr['id'] = $row->id;
			$arr['cat_id'] = $row->cat_id;
			$arr['price'] = $row->price;

			$json_data[] = $arr;

		 }

		 echo json_encode($json_data);
	}
	
}
